import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentario_app/theme/rentario_theme.dart';

class Utils {

}

showToast(String title, String message, CustomToastType type) {
  Color _backgroundColor;
  Icon _icon;
  switch (type) {
    case CustomToastType.Error:
      _backgroundColor = RentarioTheme.errorColor;
      _icon = Icon(Icons.cancel, color: Colors.white,);
    break;
    case CustomToastType.Warning:
      _backgroundColor = RentarioTheme.warningColor;
      _icon = Icon(Icons.warning, color: Colors.white,);
    break;
    case CustomToastType.Success:
      _backgroundColor = RentarioTheme.successColor;
      _icon = Icon(Icons.done, color: Colors.white,);
    break;
    default:
      _backgroundColor = RentarioTheme.baseColor;
      _icon = Icon(Icons.info, color: Colors.white,);
    break;
  }
  Get.snackbar(title, message,
    colorText: Colors.white,
    backgroundColor: _backgroundColor,
    snackPosition: SnackPosition.BOTTOM,
    icon: _icon
  );
}

enum CustomToastType {
  Error,
  Info,
  Success,
  Warning
}

ProgressDialog getLoadingDialog({BuildContext context, String text}) {
  ProgressDialog pr = ProgressDialog(
    context,
    isDismissible: false,
    type: ProgressDialogType.Normal
  );
  pr.update(message: text);
  return pr;
}