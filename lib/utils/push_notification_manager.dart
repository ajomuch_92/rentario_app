import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationsManager {
  PushNotificationsManager._();

  factory PushNotificationsManager() => _instance;

  static final PushNotificationsManager _instance = PushNotificationsManager._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;

  Future<void> init() async {
    if (!_initialized) {
      _firebaseMessaging.requestNotificationPermissions();
      _firebaseMessaging.configure(
          /*onBackgroundMessage: (Map<String, dynamic> message) async{
             print('onBackgroundMessage $message');
          },*/
          onLaunch: (Map<String, dynamic> message) async {
            print('onLaunch: $message');
          },
          onResume: (Map<String, dynamic> message) async {
            print('onResume: $message');
          },
          onMessage: (Map<String, dynamic> message) async {
            print('onMessage $message');
          }
      );
      _initialized = true;
    }
  }

  Future<String> getToken() async{
    String token = await _firebaseMessaging.getToken();
    return token;
  }
}