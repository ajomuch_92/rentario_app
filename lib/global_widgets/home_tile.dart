import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:rentario_app/theme/rentario_theme.dart';
import 'package:rentario_app/models/casa.dart';

class HomeTile extends StatelessWidget {
  final Casa casa;
  final Function() tap;
  final Function() onButtonTap;
  final Widget floatingWidget;

  const HomeTile({Key key, this.casa, this.tap, this.onButtonTap, this.floatingWidget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: tap,
        child: Column(
          children: [
            Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 200,
                  width: Get.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                    border: Border.all(color: RentarioTheme.textColor),
                    image: DecorationImage(
                      image: NetworkImage(casa.fotos[0].url),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    height: 85,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        border: Border.all(color: RentarioTheme.textColor)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(casa.tipoVivienda),
                        Row(
                          children: [
                            Text(casa.arrendatario, style: TextStyle(fontSize: 12.0),),
                            Spacer(),
                            Text('L. ${casa.precio}', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),)
                          ],
                        ),
                        Row(
                          children: [
                            RatingBar.builder(
                              initialRating: casa.promedioPuntuacion,
                              itemSize: 20.0,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemBuilder: (context, _) => Icon(
                                Ionicons.star,
                                color: Colors.amber,
                              ),
                              ignoreGestures: true,
                              onRatingUpdate: (value){},
                            ),
                            SizedBox(width: 10.0,),
                            Text('${casa.cantidadComentarios} comentarios', style: TextStyle(fontSize: 10.0, fontStyle: FontStyle.italic),)
                          ],
                        )
                      ],
                    ),
                  ),
                  right: 0,
                  left: 0,
                  bottom: -65,
                ),
                Positioned(
                  child: floatingWidget,
                  right: 10,
                  bottom: -10,
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        color: Colors.black38,
                        borderRadius: BorderRadius.all(Radius.circular(20))
                    ),
                    child: Row(
                      children: [
                        Icon(LineAwesomeIcons.map_marker, color: Colors.white,),
                        Text(casa.lugar,
                          style: TextStyle(color: Colors.white),
                          overflow: TextOverflow.fade,
                          maxLines: 1,
                          softWrap: true,
                        )
                      ],
                    ),
                  ),
                  left: 10,
                  top: 10,
                )
              ],
            ),
            SizedBox(height: 80,)
          ],
        )
    );
  }

}