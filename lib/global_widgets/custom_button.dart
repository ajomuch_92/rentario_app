import 'package:flutter/material.dart';
import 'package:rentario_app/theme/rentario_theme.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Function() tap;
  final bool disabled;
  final double width;
  final double height;
  final Color backgroundColor;
  final Color borderColor;
  final Color textColor;
  final double radius;
  final double fontSize;

  const CustomButton({Key key, @required this.text,
    @required this.tap,
    @required this.disabled,
    this.width,
    this.height,
    this.backgroundColor,
    this.borderColor,
    this.radius,
    this.textColor,
    this.fontSize}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: this.width ?? MediaQuery.of(context).size.width * 0.5,
      height: this.height ?? 30,
      child: RaisedButton(
        child: Tooltip(
          message: this.text,
          child: Text(
            this.text,
            style: TextStyle(
              fontSize: fontSize??16.0,
              color: textColor??Colors.white
            ),
            overflow: TextOverflow.ellipsis,
          ),
        ),
        onPressed: disabled? null: tap,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius??0),
          side: BorderSide(color: disabled? Colors.grey[300]: borderColor??RentarioTheme.baseColor)
        ),
        color: disabled? Colors.grey[300]: backgroundColor??RentarioTheme.baseColor,
        padding: const EdgeInsets.all(8.0),
        highlightColor: Colors.grey[50],      
      ),
    );
  }
}