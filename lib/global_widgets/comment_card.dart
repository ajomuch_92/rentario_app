import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/models/puntuacion.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class CommentCard extends StatelessWidget {
  final Puntuacion puntuacion;

  const CommentCard({Key key, this.puntuacion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      overflow: Overflow.visible,
      children: [
        Card(
          margin: EdgeInsets.only(bottom: 15.0),

          elevation: 0,
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.grey[350], width: 2.0),
            borderRadius: BorderRadius.circular(10),
          ),
          child: ListTile(
            title: ExpandableText(
              puntuacion.comentario,
              expandText: 'Mostrar más',
              collapseText: 'Mostrar menos',
              maxLines: 8,
              linkColor: Colors.blue,
            ),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RatingBar.builder(
                  initialRating: puntuacion.puntuacion.toDouble(),
                  itemSize: 15.0,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemBuilder: (context, _) => Icon(
                    Ionicons.star,
                    color: Colors.amber,
                  ),
                  ignoreGestures: true,
                  onRatingUpdate: (value){},
                ),
                Text(timeago.format(puntuacion.fecha, locale: 'es'), style: TextStyle(fontStyle: FontStyle.italic),)
              ],
            ),
          ),
        ),
        Positioned(
          child: CircleAvatar(
            radius: 15.0,
            backgroundImage: NetworkImage(puntuacion.urlPerfil),
          ),
          right: -10,
          top: -10,
        ),
      ],
    );
  }
}
