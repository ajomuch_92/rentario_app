import 'package:flutter/material.dart';
import 'package:rentario_app/theme/rentario_theme.dart';
import 'package:lottie/lottie.dart';

class NoInternet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2,
        backgroundColor: RentarioTheme.baseColor,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(100)
            )
        ),
        title: Text('Rentar.io'),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.all(18.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('No cuentas con una conexión a internet', style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
              Text('Algunas operaciones requieren de una conexión estable a internet. Es posible que luego de conectarte, tengas que esperar algunos segundos para que esta pantalla desaparezca', textAlign: TextAlign.justify,),
              Lottie.asset('assets/lotties/no-connection-internet.json')
            ],
          ),
        ),
      ),
    );
  }
}
