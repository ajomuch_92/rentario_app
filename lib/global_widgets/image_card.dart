import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'dart:io';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/models/foto.dart';

class ImageCard extends StatelessWidget {
  final Function onDeleteTap;
  final Foto image;

  const ImageCard({this.onDeleteTap, this.image});

  @override
  Widget build(BuildContext context) {
    final Uuid _uuid = Uuid();
    return Stack(
      overflow: Overflow.visible,
      children: [
        Card(
          elevation: 8,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Container(
            width: 100.0,
            height: 150.0,
            child: Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: image.file != null?
                Image.file(
                  image.file,
                  fit: BoxFit.fitWidth,
                ):
                Image.network(
                  image.url,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
        ),
        Positioned(
          child: InkWell(
            child: Icon(Ionicons.close_circle, size: 24.0, color: Colors.red[600],),
            onTap: onDeleteTap,
          ),
          right: 5,
          top: 5,
        ),
      ],
    );
  }
}
