class EstadoCita {
  int id;
  String estado, accion;

  EstadoCita({this.id, this.estado, this.accion});

  factory EstadoCita.fromJson(Map<String, dynamic> json) => EstadoCita(
    id: json['id'],
    estado: json['estado'],
    accion: json['accion']
  );
}