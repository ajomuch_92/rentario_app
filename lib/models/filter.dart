class Filter {
  int idTipoVivienda, idPais, idDivision, idSubdivision, dormitorios;
  String colonia;
  double precioInicial, precioFinal, puntuacion;

  Filter({
    this.idTipoVivienda, this.idPais, this.idDivision, this.idSubdivision,
    this.dormitorios, this.colonia, this.precioInicial,
    this.precioFinal, this.puntuacion
  });

  factory Filter.fromJson(Map<String, dynamic> json) => Filter(
    idTipoVivienda: json['idTipoVivienda'],
    idPais: json['idPais'],
    idDivision: json['idDivision'],
    idSubdivision: json['idSubdivision'],
    dormitorios: json['dormitorios'],
    colonia: json['colonia'],
    precioInicial: json['precioInicial'],
    precioFinal: json['precioFinal'],
    puntuacion: json['puntuacion']
  );

  factory Filter.fromCustomJson(Map<String, dynamic> json) => Filter(
    idTipoVivienda: json['idTipoVivienda'],
    idPais: json['idPais'],
    idDivision: json['idDivision'],
    idSubdivision: json['idSubdivision'],
    dormitorios: json['dormitorios'],
    colonia: json['colonia'],
    precioInicial: json['precio']['start'],
    precioFinal: json['precio']['end'],
    puntuacion: json['puntuacion']
  );

  Map<String, dynamic> toJson() => {
    'idTipoVivienda': idTipoVivienda,
    'idPais': idPais,
    'idDivsion': idDivision,
    'idSubdivision': idSubdivision,
    'dormitorios': dormitorios,
    'colonia': colonia,
    'precioInicial': precioInicial,
    'precioFinal': precioFinal,
    'puntuacion': puntuacion
  };

  Map<String, dynamic> toCustomJson() => {
    'idTipoVivienda': idTipoVivienda,
    'idPais': idPais,
    'idDivsion': idDivision,
    'idSubdivision': idSubdivision,
    'dormitorios': dormitorios??1,
    'colonia': colonia??'',
    'precio': {
      'start': precioInicial??0.0,
      'end': precioFinal??0.0
    },
    'puntuacion': puntuacion??0.0
  };

  bool isEmpty() {
    return idTipoVivienda == null && idPais == null && idDivision == null && idSubdivision == null && dormitorios == null
    && colonia == null && precioFinal == null && precioInicial == null && puntuacion == null;
  }
}