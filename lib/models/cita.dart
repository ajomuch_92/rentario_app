import 'package:intl/intl.dart';
import 'package:rentario_app/models/usuario.dart';

class Cita {
  int id, idVivienda, idCliente, estado;
  String comentario, comentarioArrendatario, estadoCita;
  DateTime fechaCreacion, fechaCita;
  Usuario usuario;
  bool arrendatario;

  Cita({
    this.id,
    this.idVivienda,
    this.idCliente,
    this.estado, this.comentario,
    this.fechaCreacion,
    this.fechaCita,
    this.comentarioArrendatario,
    this.estadoCita,
    this.usuario,
    this.arrendatario
  });
  
  factory Cita.fromJson(Map<String, dynamic> json) => Cita(
    id: json['id'],
    idVivienda: json['idVivienda'],
    idCliente: json['idCliente'],
    estado: json['estado'],
    comentario: json['comentario'],
    fechaCreacion: DateTime.tryParse(json['fechaCreacion']??''),
    fechaCita: DateTime.tryParse(json['fechaCita'].toString()??''),
    comentarioArrendatario: json['comentarioArrendatario'],
    estadoCita: json['estadoCita'],
    usuario: Usuario(nombre: json['cliente'], telefono: json['telefono'], urlPerfil: json['urlPerfil'], contactarPorWhatsapp: json['contactarPorWhatsapp'])
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'idVivienda': idVivienda,
    'idCliente': idCliente,
    'estado': estado,
    'comentario': comentario,
    'fechaCreacion': fechaCreacion != null? DateFormat('yyyy-MM-ddTHH:mm:ss').format(fechaCreacion): '',
    'fechaCita': fechaCita != null? DateFormat('yyyy-MM-ddTHH:mm:ss').format(fechaCita): '',
    'comentarioArrendatario': comentarioArrendatario
  };
}