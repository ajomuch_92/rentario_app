class TipoVivienda {
  int id;
  String tipoVivienda1;

  TipoVivienda({this.id, this.tipoVivienda1});

  factory TipoVivienda.fromJson(Map<String, dynamic> json) => TipoVivienda(
      id: json['id'],
      tipoVivienda1: json['tipoVivienda1']
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'TipoVivienda1': tipoVivienda1
  };
}