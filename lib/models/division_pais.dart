class DivisionPais {
  int id, idPais;
  String nombre;

  DivisionPais({this.id, this.idPais, this.nombre});

  factory DivisionPais.fromJson(Map<String, dynamic> json) => DivisionPais(
    id: json['id'],
    nombre: json['nombre'],
    idPais: json['idPais']
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'nombre': nombre,
    'idPais': idPais
  };
}