class TipoUsuario {
  int id;
  String tipoUsuario;
  bool estado;

  TipoUsuario({this.id, this.tipoUsuario, this.estado});

  factory TipoUsuario.fromJson(Map<String, dynamic> json) => TipoUsuario(
    id: json['id'] as int,
    tipoUsuario: json['tipoUsuario'],
    estado: json['estado'] as bool
  );
}