class Pais {
  int id;
  String nombre;

  Pais({this.id, this.nombre});

  factory Pais.fromJson(Map<String, dynamic> json) => Pais(
    id: json['id'],
    nombre: json['nombre']
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'nombre': nombre
  };
}