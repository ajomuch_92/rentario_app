import 'package:rentario_app/models/foto.dart';
import 'package:intl/intl.dart';

class Casa{
  int id, dormitorios, cantidadComentarios, idUsuario, idSubdivision, idTipoVivienda;
  String colonia, descripcion, arrendatario, lugar, direccionExacta, urlPerfil, tipoVivienda, telefono;
  double latitud, longitud, precio, promedioPuntuacion;
  bool banioPrivado, entradaIndependiente, estado, contactarPorWhatsapp;
  List<Foto> fotos;
  DateTime fechaPublicacion;
  //Not included on constructor
  int idPais, idDivision;
  bool favorite;

  Casa({this.id, this.dormitorios, this.cantidadComentarios, this.colonia, this.descripcion,
    this.arrendatario, this.lugar, this.latitud, this.longitud, this.precio, this.promedioPuntuacion,
    this.banioPrivado, this.entradaIndependiente, this.estado, this.fotos, this.direccionExacta,
    this.idUsuario, this.idSubdivision, this.idTipoVivienda, this.fechaPublicacion, this.urlPerfil,
    this.tipoVivienda, this.telefono, this.contactarPorWhatsapp, this.favorite
  });

  factory Casa.fromJson(Map<String, dynamic> json) => Casa(
    dormitorios: json['dormitorios'],
    colonia: json['colonia'],
    direccionExacta: json['direccionExacta'],
    latitud: json['latitud'] as double,
    longitud: json['longitud'] as double,
    descripcion: json['descripcion'],
    estado: json['estado']??json['disponible'],
    idUsuario: json['idUsuario'] as int,
    idSubdivision: json['idSubdivision'] as int,
    precio: double.parse(json['precio'].toString()??'0'),
    idTipoVivienda: json['idTipoVivienda'] as int,
    fechaPublicacion: DateTime.parse(json['fechaPublicacion']??DateTime.now().toString()),
    fotos: _getListFotos(json['fotosXVivienda']),
  );

  factory Casa.fromCustomJson(Map<String, dynamic> json) => Casa(
      id: json['id'],
      colonia: json['colonia'],
      direccionExacta: json['direccionExacta'],
      latitud: json['latitud'] as double,
      longitud: json['longitud'] as double,
      descripcion: json['descripcion'],
      dormitorios: json['dormitorios'],
      estado: json['estado'],
      idUsuario: json['idUsuario'] as int,
      idSubdivision: json['idSubdivision'] as int,
      precio: json['precio'] as double,
      idTipoVivienda: json['idTipoVivienda'] as int,
      fechaPublicacion: DateTime.parse(json['fechaPublicacion']??DateTime.now().toString()),
      urlPerfil: json['urlPerfil'],
      tipoVivienda: json['tipoVivienda'],
      arrendatario: json['arrendatario'],
      cantidadComentarios: json['cantidadComentario'] as int,
      promedioPuntuacion: json['promedioPuntuacion'] as double,
      lugar: json['lugar'],
      telefono: json['telefono'],
      contactarPorWhatsapp: json['contactarPorWhatsapp'],
      favorite: json['favorita']??false,
      fotos: _getListFotos(json['fotos'])
  );

  Map<String, dynamic> toJsonAdd() => {
    'colonia': colonia,
    'latitud': latitud,
    'longitud': longitud,
    'descripcion': descripcion,
    'estado': estado,
    'idUsuario': idUsuario,
    'idSubdivision': idSubdivision,
    'precio': precio,
    'idTipoVivienda': idTipoVivienda,
    'fechaPublicacion': DateFormat('yyyy-MM-dd hh:mm:ss').format(fechaPublicacion),
    'fotosXVivienda': _fotosToJson(),
    'dormitorios': dormitorios
  };

  Map<String, dynamic> toJsonEdit() => {
    'id': id,
    'colonia': colonia,
    'latitud': latitud,
    'longitud': longitud,
    'descripcion': descripcion,
    'estado': estado,
    'idUsuario': idUsuario,
    'idSubdivision': idSubdivision,
    'precio': precio,
    'idTipoVivienda': idTipoVivienda,
    'fechaPublicacion': DateFormat('yyyy-MM-dd hh:mm:ss').format(fechaPublicacion),
    'dormitorios': dormitorios,
    'fotosXVivienda': _fotosToJson()
  };

  List<Map<String, dynamic>> _fotosToJson() {
    List<Map<String, dynamic>> result = [];
    for(Foto foto in fotos) {
      result.add(foto.toJson());
    }
    return result;
  }
  
  static List<Foto> _getListFotos(dynamic json) {
    if(json == null)
      return [];
    List<Foto> fotos = [];
    for(Map i in json) {
      fotos.add(Foto.fromJson(i));
    }
    return fotos;
  }
}