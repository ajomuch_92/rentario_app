class ResponseResult {
  String message;
  int code;
  dynamic result;

  static int successCode = 1;
  static int errorCode = 2;

  ResponseResult({this.message, this.code, this.result});
}