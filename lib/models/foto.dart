import 'dart:io';
class Foto {
  int id, idVivienda;
  File file;
  String url;

  Foto({this.id, this.idVivienda, this.url, this.file});

  factory Foto.fromJson(Map<String, dynamic> json) => Foto(
    id: json['id'],
    idVivienda: json['idVivienda'],
    url: json['url'],
  );

  Map<String, dynamic> toJson() => {
    'url': url
  };
}