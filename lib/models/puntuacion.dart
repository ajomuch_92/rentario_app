import 'package:intl/intl.dart';

class Puntuacion {
  int id, idUsuario, idVivienda, puntuacion;
  String comentario;
  DateTime fecha;
  //Not mapped to database
  String nombreUsuario, urlPerfil;

  Puntuacion({this.id, this.idUsuario, this.idVivienda, this.puntuacion, this.comentario, this.fecha,
    this.nombreUsuario, this.urlPerfil});

  factory Puntuacion.fromJson(Map<String, dynamic> json) => Puntuacion(
    id: json['id'],
    idVivienda: json['idVivienda'],
    idUsuario: json['idUsuario'],
    comentario: json['comentario'],
    fecha: DateTime.tryParse(json['fecha']??''),
    nombreUsuario: json['nombreUsuario'],
    urlPerfil: json['urlPerfil'],
    puntuacion: json['puntuacion']
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'idVivienda': idVivienda,
    'idUsuario': idUsuario,
    'comentario': comentario,
    'fecha':  DateFormat('yyyy-MM-dd hh:mm:ss').format(fecha),
    'puntuacion': puntuacion
  };
}