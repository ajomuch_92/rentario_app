import 'package:intl/intl.dart';

class Usuario {
  String nombre, urlPerfil, correo, contrasenia, genero, fcmToken, token, telefono, contraseniaRepetida, codigoRecuperacion;
  int id, tipoUsuario;
  bool primeraVez, contactarPorWhatsapp;
  DateTime fechaNacimiento;
  double calificacion;

  Usuario({this.nombre,
    this.urlPerfil,
    this.correo,
    this.id,
    this.tipoUsuario,
    this.primeraVez,
    this.contrasenia,
    this.genero,
    this.fechaNacimiento,
    this.fcmToken,
    this.token,
    this.telefono,
    this.contactarPorWhatsapp,
    this.calificacion,
    this.contraseniaRepetida,
    this.codigoRecuperacion
  });

  factory Usuario.fromJsonApi(Map<String, dynamic> json) => Usuario(
    id: json['id'] as int,
    nombre: json['nombre'],
    urlPerfil: json['urlPerfil'],
    primeraVez: json['primeraVez'] as bool,
    correo: json['correo'],
    tipoUsuario: json['tipoUsuario'] as int
  );

  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
    id: json['id'],
    nombre: json['nombre'],
    correo: json['correo'],
    telefono: json['telefono'],
    contrasenia: json['contrasenia'],
    tipoUsuario: json['tipoUsuario'] as int,
    genero: json['genero'],
    fechaNacimiento: json['fechaNacimiento']!= null? DateTime.parse(json['fechaNacimiento'].toString()): null,
    urlPerfil: json['urlPerfil'],
    primeraVez: json['primeraVez'] as bool,
    token: json['token'],
    contactarPorWhatsapp: json['contactarPorWhatsapp']
  );

  Map<String, dynamic> toCompleteJson() => {
    'id': id,
    'nombre': nombre,
    'correo':correo,
    'telefono':telefono,
    'contrasenia':contrasenia,
    'tipoUsuario':tipoUsuario ,
    'genero':genero,
    'fechaNacimiento': fechaNacimiento!=null? DateFormat('yyyy-MM-dd').format(fechaNacimiento): null,
    'urlPerfil':urlPerfil,
    'primeraVez':primeraVez,
    'token':token,
    'contactarPorWhatsapp': contactarPorWhatsapp
};

  Map<String, dynamic> toJson() => {
    'id': id,
    'nombre': nombre,
    'urlPerfil': urlPerfil,
    'primeraVez': primeraVez,
    'correo': correo,
    'tipoUsuario': tipoUsuario
  };

  Map<String, dynamic> toEditJson() => {
    'nombre': nombre,
    'urlPerfil': urlPerfil,
    'tipoUsuario': tipoUsuario,
    'telefono':telefono,
  };

  Map<String, dynamic> toJsonRegister() => {
    'nombre': nombre,
    'correo': correo,
    'contrasenia': contrasenia,
    'genero': genero,
    'tipoUsuario': tipoUsuario,
    'fechaNacimiento': DateFormat('yyyy-MM-dd').format(fechaNacimiento)
  };

  Map<String, dynamic> toJsonLogin() => {
    'correo': correo,
    'contrasenia': contrasenia
  };

  Map<String, dynamic> toJsonRecovery() => {
    'correo': correo,
    'contrasenia': contrasenia,
    'codigoRecuperacion': codigoRecuperacion
  };

  Map<String, dynamic> toJsonUpdatePass() => {
    'id': id,
    'contrasenia': contrasenia,
    'nuevaContrasenia': contraseniaRepetida
  };
}