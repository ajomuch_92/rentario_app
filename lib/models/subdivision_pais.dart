class SubdivisionPais {
  int id, idDivision;
  String nombre;

  SubdivisionPais({this.id, this.idDivision, this.nombre});

  factory SubdivisionPais.fromJson(Map<String, dynamic> json) => SubdivisionPais(
    id: json['id'],
    nombre: json['nombre'],
    idDivision: json['idDivision']
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'nombre': nombre,
    'idPais': idDivision
  };
}