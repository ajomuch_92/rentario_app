import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:rentario_app/controllers/index_controller.dart';
import 'package:rentario_app/global_widgets/custom_button.dart';
import 'package:rentario_app/theme/rentario_theme.dart';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/views/register.dart';

class Index extends StatelessWidget {
  const Index({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<IndexController>(
      init: IndexController(),
      builder: (_) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: Get.height * 0.45,
                  padding: EdgeInsets.all(8.0),
                  decoration: BoxDecoration(
                    color: RentarioTheme.baseColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(80),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Bienvenido', style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),),
                        SizedBox(height: 10),
                        SizedBox(height: Get.height * 0.2, child: Image.asset('assets/images/logo.png')),
                        SizedBox(height: 10),
                        Text('Rentar.io', style: TextStyle(fontSize: 28)),
                      ]
                    ),
                  ),
                ),
                Container(
                  height: Get.height * 0.55,
                  padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 50.0),
                  child: FormBuilder(
                    key: _.formKey,
                    child: SingleChildScrollView(
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Inicia sesión para que comiences tu búsqueda',
                            style: TextStyle(color: Color.fromARGB(255, 162, 165, 171), fontSize: 20.0,),
                            textAlign: TextAlign.center,
                          ),
                          FormBuilderTextField(
                            attribute: 'correo',
                            initialValue: '',
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              labelText: 'Email',
                              suffixIcon: Icon(Ionicons.mail_outline)
                            ),
                            validators: [
                              FormBuilderValidators.required(errorText: 'Este campo es requerido'),
                            ],
                          ),
                          Obx(() => FormBuilderTextField(
                            attribute: 'contrasenia',
                            initialValue: '',
                            decoration: InputDecoration(
                              labelText: 'Contraseña',
                              suffixIcon: IconButton(
                                icon: _.obscureContrasenia.value? Icon(Ionicons.eye_outline): Icon(Ionicons.eye_off_outline),
                                onPressed: () {
                                  _.obscureContrasenia.value = !_.obscureContrasenia.value;
                                },
                              )
                            ),
                            obscureText: _.obscureContrasenia.value,
                            validators: [
                              FormBuilderValidators.required(errorText: 'Este campo es requerido'),
                            ],
                          )),
                          SizedBox(height: 15.0,),
                          CustomButton(
                            text: 'Ingresar',
                            tap: _.login,
                            disabled: false,
                            height: 50.0,
                            width: Get.width * 0.7,
                            radius: 10.0,
                          ),
                          SizedBox(height: 10.0,),
                          FlatButton(
                            child: Text('¿Olvidaste tu contraseña?', style: TextStyle(fontSize: 12),),
                            onPressed: _.goToRecovery,
                          ),
                          FlatButton(
                            child: Text('Crea tu cuenta', style: TextStyle(fontSize: 12)),
                            onPressed: (){
                              Get.to(Register());
                            },
                          )
                        ]
                      ),
                    )
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}