import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/controllers/filters_advanced_controller.dart';
import 'package:rentario_app/global_widgets/custom_button.dart';
import 'package:rentario_app/theme/rentario_theme.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class FiltersAdvanced extends StatelessWidget {
  const FiltersAdvanced({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FilterAdvancedController>(
      init: FilterAdvancedController(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Filtro Avanzado', style: TextStyle(color: RentarioTheme.textColor),),
            elevation: 3.0,
            leading: IconButton(
              icon: Icon(Ionicons.chevron_back, color: RentarioTheme.baseColor,),
              onPressed: Get.back,
            ),
            backgroundColor: Colors.white,
          ),
          bottomNavigationBar: Container(
            padding: EdgeInsets.all(3.0),
            child: Row(
              children: [
                CustomButton(
                  backgroundColor: Colors.white,
                  borderColor: Color.fromARGB(255, 52, 152, 219),
                  textColor: Color.fromARGB(255, 52, 152, 219),
                  text: 'Quitar Filtro',
                  disabled: false,
                  tap: _.resetFilter,
                  height: 20.0,
                  width: Get.width * 0.4,
                  radius: 10.0,
                ),
                Spacer(),
                CustomButton(
                  backgroundColor: Color.fromARGB(255, 52, 152, 219),
                  borderColor: Color.fromARGB(255, 52, 152, 219),
                  text: 'Establecer',
                  disabled: false,
                  tap: _.applyFilter,
                  height: 20.0,
                  width: Get.width * 0.4,
                  radius: 10.0,
                ),
              ],
            ),
          ),
          body: Container(
            padding: EdgeInsets.all(18.0),
            child: SingleChildScrollView(
              child: FormBuilder(
                key: _.formKey,
                initialValue: _.filter.toCustomJson(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Obx(() => FormBuilderDropdown(
                      attribute: 'idTipoVivienda',
                      decoration: InputDecoration(
                          labelText: 'Tipo',
                          prefixIcon: Icon(Ionicons.home_outline)
                      ),
                      validators: [FormBuilderValidators.required()],
                      items: _.tiposViviendas
                          .map((tipo) => DropdownMenuItem(
                          value: tipo.id,
                          child: Text("${tipo.tipoVivienda1}")
                      )).toList(),
                    )),
                    Obx(() => FormBuilderDropdown(
                      attribute: 'idPais',
                      decoration: InputDecoration(
                          labelText: 'País',
                          prefixIcon: Icon(LineAwesomeIcons.globe)
                      ),
                      onChanged: _.onChangePais,
                      validators: [FormBuilderValidators.required()],
                      items: _.paises
                          .map((pais) => DropdownMenuItem(
                          value: pais.id,
                          child: Text("${pais.nombre}")
                      )).toList(),
                    )),
                    Obx(() => FormBuilderDropdown(
                      attribute: 'idDivision',
                      decoration: InputDecoration(
                          labelText: 'Departamento',
                          prefixIcon: Icon(LineAwesomeIcons.building),
                      ),
                      onChanged: _.onChangeDivision,
                      validators: [FormBuilderValidators.required()],
                      items: _.listaDivision
                          .map((division) => DropdownMenuItem(
                          value: division.id,
                          child: Text("${division.nombre}")
                      )).toList(),
                    )),
                    Obx(() => FormBuilderDropdown(
                      attribute: 'idSubdivision',
                      decoration: InputDecoration(
                          labelText: 'Municipio',
                          prefixIcon: Icon(LineAwesomeIcons.city)
                      ),
                      validators: [FormBuilderValidators.required()],
                      items: _.listaSubDivision
                          .map((subdivision) => DropdownMenuItem(
                          value: subdivision.id,
                          child: Text("${subdivision.nombre}")
                      )).toList(),
                    )),
                    FormBuilderTextField(
                      attribute: 'colonia',
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        labelText: 'Colonia/Barrio',
                        prefixIcon: Icon(LineAwesomeIcons.alternate_store)
                      ),
                      validators: [
                        FormBuilderValidators.required(errorText: 'Este campo es obligatorio'),
                      ],
                    ),
                    FormBuilderTouchSpin(
                      decoration: InputDecoration(labelText: 'Dormitorios'),
                      attribute: 'dormitorios',
                      step: 1,
                      iconSize: 48.0,
                      addIcon: Icon(Icons.arrow_right),
                      subtractIcon: Icon(Icons.arrow_left),
                    ),
                    FormBuilderCustomField(
                      attribute: 'puntuacion',
                      formField: FormField(
                        enabled: true,
                        builder: (FormFieldState<dynamic> field) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              labelText: 'Puntuación',
                              contentPadding:
                              EdgeInsets.only(top: 10.0, bottom: 0.0),
                              border: InputBorder.none,
                              errorText: field.errorText,
                            ),
                            child: Container(
                              child: RatingBar.builder(
                                initialRating: field.value,
                                itemSize: 20.0,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemBuilder: (context, _) => Icon(
                                  Ionicons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (value){
                                  field.didChange(value);
                                },
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    FormBuilderCustomField(
                      attribute: 'precio',
                      formField: FormField(
                        enabled: true,
                        builder: (FormFieldState<dynamic> field) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              labelText: 'Precio [L ${field.value['start']}, L ${field.value['end']}]',
                              contentPadding:
                              EdgeInsets.only(top: 10.0, bottom: 0.0),
                              border: InputBorder.none,
                              errorText: field.errorText,
                            ),
                            child: Container(
                              child: FlutterSlider(
                                values: [field.value['start'], field.value['end']],
                                step: FlutterSliderStep(step: 100.0),
                                rangeSlider: true,
                                max: 30000.0,
                                min: 0.0,
                                onDragging: (handlerIndex, lowerValue, upperValue) {
                                  Map<String, double> value = {
                                    'start': lowerValue,
                                    'end': upperValue
                                  };
                                  field.didChange(value);
                                },
                              )
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}