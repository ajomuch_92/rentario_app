import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/controllers/select_place_controller.dart';
import 'package:rentario_app/theme/rentario_theme.dart';

class SelectPlace extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SelectPlaceController>(
      init: SelectPlaceController(),
      builder: (_){
        return Scaffold(
          appBar: AppBar(
            title: Text('Selecciona un lugar', style: TextStyle(color: RentarioTheme.textColor),),
            elevation: 3.0,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Ionicons.chevron_back, color: RentarioTheme.baseColor,),
              onPressed: (){
                Get.back();
              },
            ),
          ),
          body: Container(
            child: GoogleMap(
              mapType: MapType.normal,
              onMapCreated: _.onMapCreated,
              initialCameraPosition: _.initialPoint,
              markers: Set<Marker>.of(_.markers),
              myLocationEnabled: true,
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: _.savePosition,
            child: Icon(Icons.check),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
        );
      }
    );
  }
}
