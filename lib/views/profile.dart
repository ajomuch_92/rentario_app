import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rentario_app/controllers/profile_controller.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileController>(
      init: ProfileController(),
      builder: (_){
        return Container(
          child: Column(
            children: [
              Container(
                height: Get.height * 0.3,
                child: Center(
                  child: Stack(
                    children: [
                      Obx(() => CircleAvatar(
                        backgroundImage: GetUtils.isNullOrBlank(_.urlProfile.value)? 
                          AssetImage('assets/images/man.png'): NetworkImage(_.urlProfile.value),
                        radius: 100,
                      ),),
                      Positioned(
                        right: 0,
                        bottom: 0,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.cyan,
                            borderRadius: BorderRadius.all(Radius.circular(50))
                          ),
                          child: IconButton(
                            icon: Icon(Ionicons.camera_outline, color: Colors.white,),
                            onPressed: _.getProfileImage,
                          ),
                        ),
                      )
                    ],
                  )
                ),
              ),
              Obx(()=> Center(
                child: Visibility(
                  child: RaisedButton(
                    child: Text('Guardar Foto', style: TextStyle(color: Colors.white),),
                    onPressed: _.saveImageProfile,
                    color: Colors.blueAccent,
                  ),
                  visible: _.showButtonSave.value,
                ),
              )),
              Row(
                children: [
                  Expanded(
                    child: new Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                        child: Divider(
                          color: Colors.black,
                          height: 36,
                        )),
                  ),
                  Obx(() =>Text(_.userName.value, style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w600),),),
                  Expanded(
                    child: new Container(
                        margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                        child: Divider(
                          color: Colors.black,
                          height: 36,
                        )),
                  ),
                ]
              ),
              Center(
                child: Obx(() => RatingBar.builder(
                  initialRating: _.calification.value,
                  itemSize: 20.0,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemBuilder: (context, _) => Icon(
                    Ionicons.star,
                    color: Colors.amber,
                  ),
                  ignoreGestures: true,
                  onRatingUpdate: (value){},
                ))
              ),
              SizedBox(height: 10.0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FlatButton(
                    onPressed: _.showEditModal,
                    child: Column(
                      children: [
                        Icon(LineAwesomeIcons.edit, color: Colors.green,),
                        Text('Editar'),
                        Text('Información'),
                      ],
                    ),
                  ),
                  SizedBox(width: 10.0,),
                  FlatButton(
                    onPressed: _.showEditPasswordModal,
                    child: Column(
                      children: [
                        Icon(LineAwesomeIcons.key, color: Colors.lightBlue,),
                        Text('Cambiar'),
                        Text('contraseña'),
                      ],
                    ),
                  ),
                  /*SizedBox(width: 10.0,),
                  FlatButton(
                    onPressed: (){},
                    child: Column(
                      children: [
                        Icon(LineAwesomeIcons.sync_icon, color: Colors.lightBlue,),
                        Text('Actualizar'),
                        Text('datos lugares'),
                      ],
                    ),
                  ),*/
                ],
              )
            ],
          ),
        );
      }
    );
  }
}
