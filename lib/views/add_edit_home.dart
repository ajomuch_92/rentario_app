import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:rentario_app/controllers/add_edit_home_controller.dart';
import 'package:rentario_app/global_widgets/image_card.dart';
import 'package:rentario_app/theme/rentario_theme.dart';
import 'package:rentario_app/global_widgets/custom_button.dart';

class AddEditHome extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddEditHomeController>(
      init: AddEditHomeController(),
      builder: (_){
        return Scaffold(
          appBar: AppBar(
            title: Text(_.idCasa == null?'Agregar nueva casa': 'Editar casa', style: TextStyle(color: RentarioTheme.textColor),),
            elevation: 3.0,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Ionicons.chevron_back, color: RentarioTheme.baseColor,),
              onPressed: (){
                Get.back();
              },
            ),
            actions: [
              _.idCasa == null?
              Container():
              IconButton(icon: Icon(LineAwesomeIcons.comment, color: Colors.lightBlue,), onPressed: _.goToComments,)
            ],
          ),
          body: Container(
            child: FutureBuilder<int>(
              future: _.loadData(),
              builder: (context, snapshot) {
                if(snapshot.hasData) {
                  return _getForm(_);
                }
                return Center(
                  child: CircularProgressIndicator()
                );
              },
            ),
          ),
        );
      }
    );
  }

  Widget _getForm(AddEditHomeController _) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: SingleChildScrollView(
        child: FormBuilder(
          key: _.formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FormBuilderDropdown(
                attribute: 'idTipoVivienda',
                decoration: InputDecoration(
                  labelText: 'Tipo',
                  prefixIcon: Icon(Ionicons.home_outline)
                ),
                initialValue: _.currentCasa!=null? _.currentCasa.idTipoVivienda: null,
                validators: [FormBuilderValidators.required()],
                items: _.tiposViviendas
                    .map((tipo) => DropdownMenuItem(
                    value: tipo.id,
                    child: Text("${tipo.tipoVivienda1}")
                )).toList(),
              ),
              FormBuilderDropdown(
                attribute: 'idPais',
                decoration: InputDecoration(
                  labelText: 'País',
                  prefixIcon: Icon(LineAwesomeIcons.globe)
                ),
                onChanged: _.onChangePais,
                initialValue: _.currentCasa!=null? _.currentCasa.idPais: null,
                validators: [FormBuilderValidators.required()],
                items: _.paises
                    .map((pais) => DropdownMenuItem(
                    value: pais.id,
                    child: Text("${pais.nombre}")
                )).toList(),
              ),
              GetBuilder<AddEditHomeController>(
                id: 'division',
                builder: (_){
                  return FormBuilderDropdown(
                    attribute: 'idDivision',
                    decoration: InputDecoration(
                        labelText: 'Departamento/Estado',
                        prefixIcon: Icon(LineAwesomeIcons.building)
                    ),
                    onChanged: _.onChangeDivision,
                    initialValue: _.currentCasa!=null? _.currentCasa.idDivision: null,
                    validators: [FormBuilderValidators.required()],
                    items: _.listaDivision
                        .map((division) => DropdownMenuItem(
                        value: division.id,
                        child: Text("${division.nombre}")
                    )).toList(),
                  );
                },
              ),
              GetBuilder<AddEditHomeController>(
                id: 'subdivision',
                builder: (_) {
                  return FormBuilderDropdown(
                    attribute: 'idSubdivision',
                    decoration: InputDecoration(
                        labelText: 'Municipio',
                        prefixIcon: Icon(LineAwesomeIcons.city)
                    ),
                    initialValue: _.currentCasa!=null? _.currentCasa.idSubdivision: null,
                    validators: [FormBuilderValidators.required()],
                    items: _.listaSubDivision
                        .map((subdivision) => DropdownMenuItem(
                        value: subdivision.id,
                        child: Text("${subdivision.nombre}")
                    )).toList(),
                  );
                },
              ),
              FormBuilderTextField(
                attribute: 'colonia',
                textInputAction: TextInputAction.next,
                initialValue: _.currentCasa!=null? _.currentCasa.colonia: '',
                decoration: InputDecoration(
                  labelText: 'Colonia/Barrio',
                  prefixIcon: Icon(LineAwesomeIcons.alternate_store)
                ),
                validators: [
                  FormBuilderValidators.required(errorText: 'Este campo es obligatorio'),
                ],
              ),
              /*FormBuilderTextField(
                minLines: 3,
                maxLines: 5,
                attribute: 'direccionExacta',
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  labelText: 'Direccion Exacta',
                  prefixIcon: Icon(LineAwesomeIcons.map_signs)
                ),
                validators: [
                  FormBuilderValidators.required(errorText: 'Este campo es obligatorio'),
                ],
              ),*/
              Obx(() =>Row(
                children: [
                  Expanded(
                    flex: 9,
                    child: Text('Dirección: ${_.coordenadas.value??'Selecciona del mapa'}', overflow: TextOverflow.ellipsis,),
                  ),
                  Expanded(
                    flex: 1,
                    child: IconButton(
                      onPressed: _.goToMap,
                      icon: Icon(Ionicons.open_outline),
                    ),
                  )
                ],
              )),
              FormBuilderTextField(
                minLines: 3,
                maxLines: 5,
                attribute: 'descripcion',
                initialValue: _.currentCasa!=null? _.currentCasa.descripcion: '',
                textInputAction: TextInputAction.next,
                maxLength: 500,
                decoration: InputDecoration(
                  labelText: 'Descripción',
                  prefixIcon: Icon(LineAwesomeIcons.digital_tachograph),
                ),
                validators: [
                  FormBuilderValidators.required(errorText: 'Este campo es obligatorio'),
                ],
              ),
              FormBuilderTextField(
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.next,
                attribute: 'dormitorios',
                initialValue: _.currentCasa!=null? _.currentCasa.dormitorios.toString(): '',
                valueTransformer: (text) => num.tryParse(text),
                decoration: InputDecoration(
                  labelText: 'Número de dormitorios',
                  prefixIcon: Icon(LineAwesomeIcons.th),
                ),
                validators: [
                  FormBuilderValidators.required(errorText: 'Este campo es obligatorio'),
                ],
              ),
              FormBuilderTextField(
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.done,
                attribute: 'precio',
                initialValue: _.currentCasa!=null? _.currentCasa.precio.toString(): '',
                valueTransformer: (text) => num.tryParse(text),
                decoration: InputDecoration(
                  labelText: 'Precio',
                  prefixIcon: Icon(Ionicons.cash_outline)
                ),
                validators: [
                  FormBuilderValidators.required(errorText: 'Este campo es obligatorio'),
                ],
              ),
              (
                  _.currentCasa!=null?
                  FormBuilderCheckbox(
                    attribute: 'disponible',
                    initialValue: _.currentCasa.estado??false,
                    label: Text('Disponible'),
                  ): Container()
              ),
              SizedBox(height: 10.0,),
              Text('Fotos/Imágenes'),
              Container(
                height: 150.0,
                child: GetBuilder<AddEditHomeController>(
                  id: 'fotos',
                  builder: (_) {
                    return ListView.builder(
                      itemCount: _.fotos.length + 1,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        if(index == 0){
                          return Card(
                            elevation: 8,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                            child: Container(
                                width: 100.0,
                                height: 150.0,
                                child: Center(
                                  child: IconButton(
                                    icon: Icon(Icons.add_circle, color: RentarioTheme.baseColor,),
                                    onPressed: _.getImage,
                                    iconSize: 48.0,
                                  ),
                                )
                            ),
                          );
                        } else {
                          return ImageCard(
                            image: _.fotos[index - 1],
                            onDeleteTap: (){
                              _.deleteImage(index - 1);
                            },
                          );
                        }
                      }
                    );
                  },
                ),
              ),
              SizedBox(height: 10.0,),
              Center(
                child: CustomButton(
                  disabled: false,
                  tap: _.saveHome,
                  text: _.currentCasa  != null? 'Guardar cambios':'Guardar y publicar',
                  radius: 5.0,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
