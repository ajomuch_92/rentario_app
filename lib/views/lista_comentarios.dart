import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/controllers/lista_comentarios_controller.dart';
import 'package:rentario_app/global_widgets/comment_card.dart';
import 'package:rentario_app/models/puntuacion.dart';

class ListaComentarios extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ListaComentariosController>(
      init: ListaComentariosController(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Comentarios', style: TextStyle(color: Colors.black),),
            leading: IconButton(
              icon: Icon(Ionicons.chevron_back_outline, color: Colors.black,),
              onPressed: () {
                Get.back();
              },
            ),
            backgroundColor: Colors.white,
            elevation: 3.0,
          ),
          backgroundColor: Colors.white,
          body: Container(
            padding: EdgeInsets.all(15.0),
            child: FutureBuilder<List<Puntuacion>>(
              future: _.getListComment(),
              builder: (context, snap) {
                if(snap.hasData) {
                  return ListView.builder(
                    itemCount: snap.data.length,
                    itemBuilder: (context, index) => CommentCard(puntuacion: snap.data[index],)
                  );
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
          ),
        );
      }
    );
  }
}
