import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/controllers/notifications_controller.dart';
import 'package:rentario_app/models/cita.dart';
import 'package:rentario_app/theme/rentario_theme.dart';
import 'package:timeago/timeago.dart' as timeago;

class Notifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<NotificationsController>(
      init: NotificationsController(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Citas y Notificaciones', style: TextStyle(color: RentarioTheme.textColor),),
            elevation: 3.0,
            leading: IconButton(
              icon: Icon(Ionicons.chevron_back, color: RentarioTheme.baseColor,),
              onPressed: (){
                Get.back();
              },
            ),
            backgroundColor: Colors.white,
          ),
          body: Container(
            padding: EdgeInsets.all(18.0),
            child: PagedListView<int, Cita>(
              pagingController: _.pagingController,
              builderDelegate: PagedChildBuilderDelegate(
                noItemsFoundIndicatorBuilder: (context) => Text('Sin datos para mostrar'),
                itemBuilder: (context, item, index) => Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.grey[350], width: 2.0),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: ListTile(
                    title: Text(item.comentario??'Sin comentario'),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Establecida para: ' + DateFormat('dd/MM/yyyy hh:mm a').format(item.fechaCita)),
                        Text(timeago.format(item.fechaCreacion, locale: 'es').capitalize),
                        Text(item.estadoCita)
                      ],
                    ),
                    leading: InkWell(
                      onTap: () {
                        _.showProfile(item.usuario);
                      },
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(item.usuario.urlPerfil),
                      ),
                    ),
                    trailing: Visibility(
                      visible: item.estado != 5,
                      child: PopupMenuButton(
                        icon: Icon(Ionicons.ellipsis_vertical),
                        itemBuilder: (context) => _.getMenuOptions(),
                        onSelected: (value) {
                          _.onMenuSelected(item, value);
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      }
    );
  }
}
