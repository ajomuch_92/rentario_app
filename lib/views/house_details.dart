import 'package:auto_size_text/auto_size_text.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rentario_app/controllers/house_details_controller.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:rentario_app/theme/rentario_theme.dart';
import 'package:rentario_app/global_widgets/custom_button.dart';

class HouseDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HouseDetailsController>(
      init: HouseDetailsController(),
      builder: (_) {
        return Scaffold(
          bottomNavigationBar: Visibility(
            visible: _.usuario.tipoUsuario == 2,
            child: Container(
              height: 50.0,
              padding: EdgeInsets.all(5.0),
              child: Row(
                children: [
                  CustomButton(
                    backgroundColor: Colors.white,
                    borderColor: Color.fromARGB(255, 52, 152, 219),
                    textColor: Color.fromARGB(255, 52, 152, 219),
                    text: 'Calificar',
                    disabled: false,
                    tap: _.showCommentModal,
                    height: 20.0,
                    width: Get.width * 0.4,
                    radius: 10.0,
                  ),
                  Spacer(),
                  CustomButton(
                    backgroundColor: Color.fromARGB(255, 52, 152, 219),
                    borderColor: Color.fromARGB(255, 52, 152, 219),
                    text: 'Hacer cita',
                    disabled: false,
                    tap: _.showAppointmentModal,
                    height: 20.0,
                    width: Get.width * 0.4,
                    radius: 10.0,
                  ),
                ],
              ),
            ),
          ),
          body: Stack(
            overflow: Overflow.visible,
            children: [
              Container(
                height: Get.height * 0.45,
                decoration: BoxDecoration(
                  color: RentarioTheme.baseColor,
                ),
                child: CarouselSlider.builder(
                  itemCount: _.casa.fotos.length,
                  itemBuilder: (context, index) {
                    return Container(
                      width: Get.width,
                      child: Image.network(_.casa.fotos[index].url, fit: BoxFit.fill),
                    );
                  },
                  options: CarouselOptions(
                    autoPlay: true,
                    aspectRatio: 1.0,
                    enlargeCenterPage: false,
                    height: Get.height * 0.45
                  ),
                ),
              ),
              Positioned(
                child: CircleAvatar(
                  backgroundColor: Colors.black,
                  child: IconButton(
                    icon: Icon(Ionicons.chevron_back),
                    onPressed: (){
                      Get.back();
                    },
                  ),
                ),
                left: 10,
                top: 40,
              ),
              Visibility(
                visible: _.usuario.tipoUsuario == 2,
                child: Positioned(
                  child: Icon(Ionicons.heart, color: _.casa.favorite? Colors.redAccent: Colors.white,),
                  right: 10,
                  top: 40,
                ),
              ),
              Positioned(
                child: Container(
                  height: Get.height * 0.75,
                  width: Get.width,
                  padding: EdgeInsets.all(18.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0))
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(3.0),
                          decoration: BoxDecoration(
                              color: Colors.black38,
                              borderRadius: BorderRadius.all(Radius.circular(20))
                          ),
                          child: Row(
                            children: [
                              Icon(LineAwesomeIcons.map_marker, color: Colors.white,),
                              Expanded(
                                child: Text(_.casa.lugar,
                                  style: TextStyle(color: Colors.white),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 10,),
                        Row(
                          children: [
                            Expanded(
                              child: AutoSizeText(_.casa.tipoVivienda,
                                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
                                maxLines: 2,
                              ),
                            ),
                            Spacer(),
                            AvatarGlow(
                              glowColor: Colors.green,
                              endRadius: 50.0,
                              duration: Duration(milliseconds: 2000),
                              startDelay: Duration(milliseconds: 500),
                              repeat: true,
                              showTwoGlows: true,
                              repeatPauseDuration: Duration(milliseconds: 100),
                              child: Material(
                                elevation: 8.0,
                                shape: CircleBorder(),
                                child: InkWell(
                                  onTap: _.showProfile,
                                  child: CircleAvatar(
                                    backgroundImage: GetUtils.isNullOrBlank(_.casa.urlPerfil)?
                                    AssetImage('assets/images/man.png'):
                                    NetworkImage(_.casa.urlPerfil),
                                  ),
                                )
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Icon(Ionicons.bed_outline),
                            Text('${(_.casa.dormitorios??0)} dormitorios'),
                            SizedBox(width: 5.0,),
                            Icon(LineAwesomeIcons.dollar_sign),
                            Text('${(_.casa.precio??0)}'),
                          ],
                        ),
                        Text(_.casa.arrendatario),
                        RatingBar.builder(
                          initialRating: _.casa.promedioPuntuacion,
                          itemSize: 20.0,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemBuilder: (context, _) => Icon(
                            Ionicons.star,
                            color: Colors.amber,
                          ),
                          ignoreGestures: true,
                          onRatingUpdate: (value){},
                        ),
                        SizedBox(height: 10,),
                        Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(width: 1.0, color: Colors.blueGrey),
                              bottom: BorderSide(width: 1.0, color: Colors.blueGrey),
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: _.goToComments,
                                child: Column(
                                  children: [
                                    Icon(LineAwesomeIcons.comment, color: Colors.lightBlue,),
                                    Text('Comentarios', style: TextStyle(color: Colors.lightBlue),)
                                  ],
                                ),
                              ),
                              SizedBox(width: 10.0),
                              InkWell(
                                onTap: _.openMap,
                                child: Column(
                                  children: [
                                    Icon(LineAwesomeIcons.alternate_map_marker, color: Colors.lightBlue,),
                                    Text('Ver Mapa', style: TextStyle(color: Colors.lightBlue),)
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Text('Descripción', style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),),
                        Text(_.casa.descripcion),
                      ],
                    ),
                  ),
                ),
                left: 0,
                top: Get.height * 0.45 - 50,
              ),
              Align(
                alignment: Alignment.topCenter,
                child: _getTitle(_),
              ),
            ],
          ),
        );
      }
    );
  }

  Widget _getTitle(HouseDetailsController _) {
    if(_.casa.estado) {
      return Container(
        margin: EdgeInsets.only(top: 50.0),
        padding: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          color: Colors.green,
          borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        child: Text('Disponible',style: TextStyle(color: Colors.white, fontSize: 12.0),),
      );
    }
    return Container(
      margin: EdgeInsets.only(top: 50.0),
      padding: EdgeInsets.all(5.0),
      decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.all(Radius.circular(20))
      ),
      child: Text('Ocupada',style: TextStyle(color: Colors.white, fontSize: 12.0),),
    );
  }
}
