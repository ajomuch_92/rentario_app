import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:rentario_app/global_widgets/custom_button.dart';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/controllers/register_controller.dart';
import 'package:rentario_app/theme/rentario_theme.dart';

class Register extends StatelessWidget {
  const Register({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterController>(
      init: RegisterController(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Ionicons.chevron_back_outline, color: Colors.black,),
              onPressed: _.back,
            ),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
          ),
          backgroundColor: Colors.white,
          body: Container(
            padding: EdgeInsets.all(30.0),
            child: Center(
              child: SingleChildScrollView(
                child: FormBuilder(
                  key: _.formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        child: Icon(Ionicons.person_add_outline, size: 56.0, color: Colors.white,),
                        backgroundColor: RentarioTheme.baseColor,
                        radius: 56.0,
                      ),
                      _.separator,
                      Text('Registrate ahora', style: TextStyle(fontSize: 32)),
                      _.separator,
                      Text('Sencillo, rápido y gratis'),
                      _.separator,
                      FormBuilderTextField(
                        keyboardType: TextInputType.name,
                        attribute: 'nombre',
                        initialValue: '',
                        decoration: InputDecoration(
                          labelText: 'Nombre completo',
                          suffixIcon: Icon(Ionicons.person_outline)
                        ),
                        validators: [
                          FormBuilderValidators.required(errorText: 'Este campo es requerido'),
                        ],
                        textCapitalization: TextCapitalization.words,
                      ),
                      _.separator,
                      FormBuilderTextField(
                        keyboardType: TextInputType.emailAddress,
                        attribute: 'correo',
                        initialValue: '',
                        decoration: InputDecoration(
                          labelText: 'Correo',
                            suffixIcon: Icon(Ionicons.mail_outline)
                        ),
                        validators: [
                          FormBuilderValidators.required(errorText: 'Este campo es requerido'),
                          FormBuilderValidators.email(errorText: 'Debes ingresar una correo válido')
                        ],

                      ),
                      _.separator,
                      FormBuilderTextField(
                        keyboardType: TextInputType.emailAddress,
                        attribute: 'contrasenia',
                        initialValue: '',
                        decoration: InputDecoration(
                          labelText: 'Contraseña',
                          suffixIcon: Icon(Ionicons.eye_outline)
                        ),
                        validators: [
                          FormBuilderValidators.required(errorText: 'Este campo es requerido'),
                          FormBuilderValidators.minLength(5, errorText: 'La contraseña debe ser de al menos 5 caracteres')
                        ],
                        obscureText: true,
                      ),
                      _.separator,
                      FormBuilderDateTimePicker(
                        attribute: 'fechaNacimiento',
                        inputType: InputType.date,
                        format: DateFormat('dd/MM/yyyy'),
                        decoration: InputDecoration(
                          labelText: 'Fecha de Nacimiento',
                          suffixIcon: Icon(Ionicons.calendar_outline)
                        ),
                        cancelText: 'Cancelar',
                        confirmText: 'Aceptar',
                        validators: [
                          FormBuilderValidators.required(errorText: 'Este campo es requerido')
                        ],
                      ),
                      _.separator,
                      FormBuilderDropdown(
                        attribute: 'genero',
                        decoration: InputDecoration(
                          labelText: 'Género'
                        ),
                        validators: [FormBuilderValidators.required(errorText: 'Este campo es requerido')],
                        items: ['Masculino', 'Femenino', 'Otro']
                            .map((gender) => DropdownMenuItem(
                            value: gender[0],
                            child: Text("$gender")
                        )).toList(),
                      ),
                      _.separator,
                      Obx(() =>FormBuilderDropdown(
                        attribute: 'tipoUsuario',
                        decoration: InputDecoration(
                            labelText: 'Soy/Estoy...'
                        ),
                        validators: [FormBuilderValidators.required(errorText: 'Este campo es requerido')],
                        items: (_.tiposUsuarios.value??[])
                            .map((type) => DropdownMenuItem(
                            value: type.id,
                            child: Text('${type.tipoUsuario}')
                        )).toList(),
                      )),
                      _.separator,
                      CustomButton(
                        backgroundColor: Color.fromARGB(255, 52, 152, 219),
                        borderColor: Color.fromARGB(255, 52, 152, 219),
                        text: 'Registrarse',
                        disabled: false,
                        tap: _.login,
                        height: 20.0,
                        width: Get.width * 0.5,
                        radius: 10.0,
                      ),
                    ]
                  ),
                )
              )
            ),
          ),
        );
      }
    );
  }
}