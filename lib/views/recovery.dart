import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pin_code_fields/flutter_pin_code_fields.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:rentario_app/controllers/recovery_controller.dart';

class Recovery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RecoveryController>(
      init: RecoveryController(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Ionicons.chevron_back_outline, color: Colors.black,),
              onPressed: Get.back,
            ),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            title: Text('Recuperación de contraseña', style: TextStyle(color: Colors.black),),
          ),
          backgroundColor: Colors.white,
          body: Container(
            padding: EdgeInsets.all(18.0),
            child: Obx(() => _getBody(_)),
          ),
        );
      }
    );
  }

  Widget _getBody(RecoveryController _) {
    if(_.step.value == 0) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Ingresa el correo de tu cuenta',style: TextStyle(fontSize: 20.0), textAlign: TextAlign.center,),
          SizedBox(height: 10.0,),
          TextField(
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                labelText: 'Correo Electrónico',
                suffixIcon: Icon(LineAwesomeIcons.mail_bulk),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black12)
                )
            ),
            onChanged: _.emailChange,
          ),
          FlatButton(
            child: new Text('Avanzar'),
            onPressed: _.getCode,
          ),
        ],
      );
    } else if(_.step.value == 1) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Ingresa el código de 5 caracteres que fue enviado a tu correo',style: TextStyle(fontSize: 20.0), textAlign: TextAlign.center,),
          SizedBox(height: 10.0,),
          PinCodeFields(
            keyboardType: TextInputType.text,
            length: 5,
            onChange: _.codeChange,
          ),
          FlatButton(
            child: new Text('Verificar'),
            onPressed: _.verifyCode,
          ),
        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Ingresa tu nueva contraseña',style: TextStyle(fontSize: 20.0), textAlign: TextAlign.center,),
          SizedBox(height: 10.0,),
          TextField(
            textInputAction: TextInputAction.done,
            obscureText: true,
            decoration: InputDecoration(
                labelText: 'Nueva contraseña',
                suffixIcon: Icon(LineAwesomeIcons.key),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black12)
                )
            ),
            onChanged: _.passwordChange,
          ),
          FlatButton(
            child: new Text('Guardar'),
            onPressed: _.resetPassword,
          ),
        ],
      );
    }
  }
}
