import 'package:flutter/material.dart';
import 'package:flutter_snake_navigationbar/flutter_snake_navigationbar.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:rentario_app/controllers/home_controller.dart';
import 'package:rentario_app/models/casa.dart';
import 'package:rentario_app/theme/rentario_theme.dart';
import 'package:rentario_app/global_widgets/home_tile.dart';
import 'package:rentario_app/views/add_edit_home.dart';
import 'package:rentario_app/views/house_details.dart';
import 'package:rentario_app/views/profile.dart';

class Home extends StatelessWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            elevation: 2,
            backgroundColor: RentarioTheme.baseColor,
            shape: ContinuousRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(100)
              )
            ),
            title: Text('Rentar.io'),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(Ionicons.notifications_outline),
                onPressed: _.goToNotifications,
              ),IconButton(
                icon: Icon(Ionicons.log_out_outline),
                onPressed: _.logout,
              ),
            ],
          ),
          body: AnimatedContainer(
            duration: Duration(milliseconds: 500),
            child: PageView(
              controller: _.pageController,
              onPageChanged: (index) {
                _.activePage.value = index;
              },
              children: [
                _getHomePage(_),
                _.usuario.tipoUsuario == 1? _getMyList(_): _getMyFavoriteHouses(_),
                Profile(),
              ],
            ),
          ),
          bottomNavigationBar: Obx(() => SnakeNavigationBar.color(
            behaviour: SnakeBarBehaviour.floating,
            snakeShape: SnakeShape.circle,
            padding: EdgeInsets.only(bottom: 2.0, left: 12.0, right: 12.0),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            showSelectedLabels: false,
            showUnselectedLabels: false,
            currentIndex: _.activePage.value,
            onTap: (index) {
              _.pageController.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.easeOut);
              _.activePage.value = index;
            },
            snakeViewColor: RentarioTheme.baseColor,
            unselectedItemColor: RentarioTheme.baseColor,
            elevation: 3.0,
            items: [
              BottomNavigationBarItem(icon: Icon(Ionicons.home), label: 'Inicio'),
              _.usuario.tipoUsuario == 1?
                BottomNavigationBarItem(icon: Icon(Ionicons.list_circle_outline), label: 'Lista'):
                BottomNavigationBarItem(icon: Icon(Ionicons.heart), label: 'Favoritos'),
              BottomNavigationBarItem(icon: Icon(Ionicons.person), label: 'Perfil'),
            ],
          )),
        );
      },
    );
  }

  Widget _getHomePage(HomeController _) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Bienvenid@ ${_.usuario.nombre.split(' ')[0]}', style: TextStyle(fontSize: 20, color: Colors.black87),),
                  _.usuario.tipoUsuario == 1?
                  Text('¿List@ para publicar?', style: TextStyle(fontWeight: FontWeight.bold, color: RentarioTheme.textColor)):
                  Text('¿List@ para buscar?', style: TextStyle(fontWeight: FontWeight.bold, color: RentarioTheme.textColor))
                ],
              ),
              Spacer(),
              Visibility(
                visible: _.usuario.tipoUsuario == 1,
                child: IconButton(
                  icon: Icon(LineAwesomeIcons.plus_square, color: RentarioTheme.baseColor, size: 40.0,),
                  onPressed: (){
                    Get.to(AddEditHome()).then((value){
                      if(value != null)
                        _.pagingController.refresh();
                    });
                  },
                ),
              ),
              IconButton(
                icon: Icon(Ionicons.reload, color: RentarioTheme.baseColor, size: 36.0,),
                onPressed: _.pagingController.refresh,
              )
            ],
          ),
          SizedBox(height: 10.0,),
          Row(
            children: [
              Flexible(
                flex: 9,
                child: TextField(
                  textInputAction: TextInputAction.search,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Buscar',
                    prefixIcon: Icon(Ionicons.search),
                    isDense: true,
                  ),
                  style: TextStyle(
                    height: 1.0
                  ),
                  onChanged: _.searchChange,
                ),
              ),
              Flexible(
                flex: 1,
                child: IconButton(
                  icon: Icon(LineAwesomeIcons.horizontal_sliders),
                  onPressed: _.goToFilters,
                ),
              ),
            ],
          ),
          SizedBox(height: 10.0,),
          _getListHouses(_)
        ],
      ),
    );
  }

  Widget _getListHouses(HomeController _) {
    return Expanded(
      child: GetBuilder<HomeController>(
        id: 'main',
        builder: (_) => PagedListView<int, Casa>(
          pagingController: _.pagingController,
          builderDelegate: PagedChildBuilderDelegate<Casa>(
            noItemsFoundIndicatorBuilder: (context) => Text('Sin datos para mostrar'),
            itemBuilder: (context, item, index) => HomeTile(
              casa: item,
              floatingWidget: _.usuario.tipoUsuario == 1? Container(): FloatingActionButton(
                child: Icon(item.favorite? Ionicons.heart: Ionicons.heart_outline, color: Colors.redAccent,),
                onPressed: () {
                  if(!item.favorite) {
                    _.addFavorite(item);
                  } else {
                    _.removeFavorite(item);
                  }
                },
                backgroundColor: Colors.white,
                heroTag: _.uuid.v1(),
              ),
              tap: () {
                Get.to(HouseDetails(), arguments: {'casa': item});
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _getMyList(HomeController _) {
    return Container(
      padding: EdgeInsets.all(18.0),
      child: Column(
        children: [
          Row(
            children: [
              Text('Mi lista de casas', style: TextStyle(fontWeight: FontWeight.bold, color: RentarioTheme.textColor)),
              Spacer(),
              IconButton(
                icon: Icon(Ionicons.reload, color: RentarioTheme.baseColor, size: 36.0,),
                onPressed: _.pagingOwnHouseController.refresh,
              )
            ],
          ),
          _getMyListHouses(_),
        ],
      ),
    );
  }

  Widget _getMyListHouses(HomeController _) {
    return Expanded(
      child: PagedListView<int, Casa>(
        pagingController: _.pagingOwnHouseController,
        builderDelegate: PagedChildBuilderDelegate(
          noItemsFoundIndicatorBuilder: (context) => Text('Sin datos para mostrar'),
          itemBuilder: (context, item, index) => HomeTile(
            casa: item,
            floatingWidget: FloatingActionButton(
              child: Icon(Ionicons.pencil, color: Colors.orange,),
              onPressed: () {
                _.editHome(item.id);
              },
              backgroundColor: Colors.white,
              heroTag: _.uuid.v1(),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getMyFavoriteHouses(HomeController _) {
    return Container(
      padding: EdgeInsets.all(18.0),
      child: Column(
        children: [
          Row(
            children: [
              Text('Mis favoritas', style: TextStyle(fontWeight: FontWeight.bold, color: RentarioTheme.textColor)),
              Spacer(),
              IconButton(
                icon: Icon(Ionicons.reload, color: RentarioTheme.baseColor, size: 36.0,),
                onPressed: _.favoriteHouseController.refresh,
              )
            ],
          ),
          _getMyFavorite(_),
        ],
      ),
    );
  }

  Widget _getMyFavorite(HomeController _) {
    return Expanded(
      child: GetBuilder<HomeController>(
        id: 'favorites',
        builder: (_) => PagedListView<int, Casa>(
          pagingController: _.favoriteHouseController,
          builderDelegate: PagedChildBuilderDelegate(
            noItemsFoundIndicatorBuilder: (context) => Text('Sin datos para mostrar'),
            itemBuilder: (context, item, index) => HomeTile(
              casa: item,
              floatingWidget: FloatingActionButton(
                child: Icon(Ionicons.heart, color: Colors.red,),
                onPressed: () {
                  _.removeFavorite(item);
                },
                backgroundColor: Colors.white,
                heroTag: _.uuid.v1(),
              ),
              tap: () {
                Get.to(HouseDetails(), arguments: {'casa': item});
              },
            ),
          ),
        ),
      ),
    );
  }
}
