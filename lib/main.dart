import 'package:cross_connectivity/cross_connectivity.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rentario_app/controllers/global_controller.dart';
import 'package:rentario_app/models/usuario.dart';
import 'package:rentario_app/utils/push_notification_manager.dart';
import 'package:rentario_app/views/home.dart';
import 'package:rentario_app/views/index.dart';
import 'package:rentario_app/global_widgets/no_internet.dart';
import 'package:get_storage/get_storage.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init('rentario');
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  GlobalController _globalController = Get.put(GlobalController());
  PushNotificationsManager _pushNotificationsManager = PushNotificationsManager();

  @override
  Widget build(BuildContext context) {
    _globalController.setDev(true);
    Usuario _usuario = _globalController.usuario;
    _pushNotificationsManager.init();
    _pushNotificationsManager.getToken().then((value) =>
      _globalController.fcmToken = value);
    return GetMaterialApp(
      title: 'Rentario',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Montserrat',
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: ConnectivityBuilder(
        builder: (context, isConnected, status) {
          if(isConnected == null) {
            return Container(color: Colors.white,child: Center(child: CircularProgressIndicator()));
          } else if(isConnected) {
            return _usuario == null ? Index(): Home();
          } else {
            return NoInternet();
          }
        }
      ),
    );
  }
}
