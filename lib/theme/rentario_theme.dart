import 'dart:ui';

class RentarioTheme {
  static Color baseColor = Color.fromARGB(255, 107, 197, 127);
  static Color successColor = Color.fromARGB(255, 39, 174, 96);
  static Color warningColor = Color.fromARGB(255, 243, 156, 18);
  static Color errorColor = Color.fromARGB(255, 231, 76, 60);
  static Color textColor = Color.fromARGB(255, 142, 133, 133);
}