import 'package:get/get.dart';
import 'package:rentario_app/controllers/global_controller.dart';
import 'package:rentario_app/models/puntuacion.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/data/provider/puntuacion_provider.dart';

class ListaComentariosController extends GetxController {
  final PuntuacionProvider _puntuacionProvider = PuntuacionProvider();
  final GlobalController _globalController = Get.find();

  Future<List<Puntuacion>> getListComment() async {
    List<Puntuacion> list = [];
    int idVivienda = Get.arguments['id'];
    ResponseResult result = await _puntuacionProvider.getPuntuaciones(idVivienda, _globalController.token);
    if(result.code == ResponseResult.successCode) {
      list = result.result;
    }
    return list;
  }
}