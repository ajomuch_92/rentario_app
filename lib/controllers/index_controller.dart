import 'package:flutter/cupertino.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentario_app/data/provider/login_provider.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/usuario.dart';
import 'package:rentario_app/utils/Utils.dart';
import 'package:rentario_app/views/home.dart';
import 'package:rentario_app/views/recovery.dart';

import 'global_controller.dart';
import 'package:rentario_app/data/provider/usuario_provider.dart';

class IndexController extends GetxController {
  final GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  LoginProvider _loginProvider = LoginProvider();
  GlobalController _globalController = Get.find();
  RxBool obscureContrasenia = true.obs;

  login() async {
    if(formKey.currentState.saveAndValidate()) {
      Map<String, dynamic> value = formKey.currentState.value;
      Usuario _usuario = Usuario.fromJson(value);
      _usuario.correo = _usuario.correo.trim();
      ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Entrando');
      await pr.show();
      ResponseResult result = await _loginProvider.login(_usuario);
      await pr.hide();
      if(result.code == ResponseResult.successCode) {
        formKey.currentState.reset();
        Usuario _usuario2 = result.result as Usuario;
        Usuario _user = Usuario(id: _usuario2.id, fcmToken: _globalController.fcmToken);
        await UsuarioProvider()..updateProfileData(_user, _usuario2.token);
        _globalController.setUser(_usuario2);
        Get.offAll(Home());
      } else {
        showToast('Inicio Sesión', 'Contraseña y/o usuario incorrectos', CustomToastType.Error);
      }
    }
  }

  void goToRecovery() {
    Get.to(Recovery()).then((value){
      if(value != null) {
        showToast('Recuperación', 'Tu contraseña fue reestablecida con éxito', CustomToastType.Info);
      }
    });
  }
}