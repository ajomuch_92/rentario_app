import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/controllers/global_controller.dart';
import 'package:rentario_app/data/provider/usuario_provider.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/usuario.dart';
import 'package:image_picker_gallery_camera/image_picker_gallery_camera.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as Path;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentario_app/utils/Utils.dart';
import 'package:rentario_app/global_widgets/custom_button.dart';

class ProfileController extends GetxController {
  GlobalController _globalController = Get.find();
  UsuarioProvider _usuarioProvider = UsuarioProvider();
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();
  final GlobalKey<FormBuilderState> _formKeyPassword = GlobalKey<FormBuilderState>();
  Usuario usuario;
  RxBool showButtonSave = false.obs;
  RxString urlProfile = ''.obs;
  RxString userName = ''.obs;
  File _profileImage;
  String _token;
  RxDouble calification = (0.0).obs;
  RxBool _showMyPassword = true.obs;
  RxBool _showNewPassword = true.obs;

  @override
  void onInit() {
    super.onInit();
    usuario = _globalController.usuario;
    _token = _globalController.token;
    urlProfile.value = usuario.urlPerfil;
    userName.value = usuario.nombre;
    getCalification();
  }

  Future<void> getProfileImage() async {
    _profileImage = await ImagePickerGC.pickImage(
        context: Get.overlayContext,
        source: ImgSource.Both,
        imageQuality: 50,
        galleryText: Text('Galería'),
        cameraText: Text('Cámara')
    );
    if (_profileImage != null) {
      showButtonSave.value = true;
      showToast('Foto de perfil', 'Guarda los cambios para que veas los resultados', CustomToastType.Info);
    }
  }

  void getCalification() async{
    ResponseResult result = await _usuarioProvider.getCalification(_globalController.idUsuario, _token);
    if(result.code == ResponseResult.successCode) {
      calification.value = double.parse(result.result.toString());
    }
  }

  Future<void> saveImageProfile() async{
    ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Actualizando...');
    await pr.show();
    Reference storageReference = FirebaseStorage.instance
        .ref()
        .child('profiles/${Path.basename(_profileImage.path)}');
    UploadTask uploadTask = storageReference.putFile(_profileImage);
    await Future.value(uploadTask);
    String _url = await storageReference.getDownloadURL();
    Usuario _usuario = Usuario(urlPerfil: _url, id: _globalController.idUsuario);
    ResponseResult result = await _usuarioProvider.updateProfilePicture(_usuario, _token);
    await pr.hide();
    if(result.code == ResponseResult.successCode) {
      usuario.urlPerfil = _url;
      urlProfile.value = _url;
      showButtonSave.value = false;
      _globalController.setUser(usuario);
    } else {
      showToast('Foto de perfil', 'Hubo un error al actualizar tu foto de perfil', CustomToastType.Error);
    }
  }

  void showEditModal() {
    showModalBottomSheet(
      context: Get.overlayContext,
      isScrollControlled: true,
      isDismissible: true,
      builder: (context) => Container(
        padding: EdgeInsets.all(18.0),
        height: Get.height * 0.9,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0), topRight: Radius.circular(50.0)),
          color: Colors.white,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Edita algunos de tus datos', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                IconButton(
                  icon: Icon(Ionicons.close),
                  onPressed: () => Get.back(),
                )
              ],
            ),
            FormBuilder(
              key: _formKey,
              child: Column(
                children: [
                  FormBuilderTextField(
                    keyboardType: TextInputType.name,
                    attribute: 'nombre',
                    initialValue: usuario.nombre,
                    decoration: InputDecoration(
                        labelText: 'Nombre completo',
                        suffixIcon: Icon(Ionicons.person_outline)
                    ),
                    validators: [
                      FormBuilderValidators.required(errorText: 'Este campo es requerido'),
                    ],
                    textCapitalization: TextCapitalization.words,
                  ),
                  SizedBox(height: 10.0,),
                  FormBuilderTextField(
                    keyboardType: TextInputType.phone,
                    attribute: 'telefono',
                    initialValue: usuario.telefono,
                    decoration: InputDecoration(
                        labelText: 'Teléfono',
                        suffixIcon: Icon(Ionicons.call_outline)
                    ),
                    textCapitalization: TextCapitalization.words,
                  ),
                  SizedBox(height: 10.0,),
                  FormBuilderDateTimePicker(
                    attribute: 'fechaNacimiento',
                    inputType: InputType.date,
                    format: DateFormat('dd/MM/yyyy'),
                    initialValue: usuario.fechaNacimiento,
                    decoration: InputDecoration(
                        labelText: 'Fecha de Nacimiento',
                        suffixIcon: Icon(Ionicons.calendar_outline)
                    ),
                    cancelText: 'Cancelar',
                    confirmText: 'Aceptar'
                  ),
                  SizedBox(height: 10.0,),
                  FormBuilderDropdown(
                    attribute: 'genero',
                    decoration: InputDecoration(
                        labelText: 'Género'
                    ),
                    initialValue: usuario.genero,
                    validators: [FormBuilderValidators.required(errorText: 'Este campo es requerido')],
                    items: ['Masculino', 'Femenino', 'Otro']
                        .map((gender) => DropdownMenuItem(
                        value: gender[0],
                        child: Text("$gender")
                    )).toList(),
                  ),
                  SizedBox(height: 10.0,),
                  FormBuilderCheckbox(
                    attribute: 'contactarPorWhatsapp',
                    initialValue: usuario.contactarPorWhatsapp??false,
                    label: Text('Permitir que me contacten por Whatsapp'),
                  )
                ],
              ),
            ),
            CustomButton(
              backgroundColor: Color.fromARGB(255, 52, 152, 219),
              borderColor: Color.fromARGB(255, 52, 152, 219),
              text: 'Guardar Cambios',
              disabled: false,
              tap: _updateProfile,
              height: 20.0,
              width: Get.width * 0.5,
              radius: 10.0,
            ),
          ],
        ),
      )
    );
  }

  void showEditPasswordModal() {
    showModalBottomSheet(
        context: Get.overlayContext,
        isScrollControlled: true,
        isDismissible: true,
        builder: (context) => Container(
          padding: EdgeInsets.all(18.0),
          height: Get.height * 0.9,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0), topRight: Radius.circular(50.0)),
            color: Colors.white,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Cambia tu contraseña', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                  IconButton(
                    icon: Icon(Ionicons.close),
                    onPressed: () => Get.back(),
                  )
                ],
              ),
              FormBuilder(
                key: _formKeyPassword,
                child: Column(
                  children: [
                    Obx(() => FormBuilderTextField(
                      keyboardType: TextInputType.text,
                      obscureText: _showMyPassword.value,
                      attribute: 'contrasenia',
                      initialValue: '',
                      decoration: InputDecoration(
                          labelText: 'Escriba su actual contraseña',
                          suffixIcon: IconButton(
                            icon: _showMyPassword.value? Icon(Ionicons.eye_outline): Icon(Ionicons.eye_off_outline),
                            onPressed: () {
                              _showMyPassword.value = !_showMyPassword.value;
                            },
                          )
                      ),
                      validators: [
                        FormBuilderValidators.required(errorText: 'Este campo es requerido'),
                      ],
                      textCapitalization: TextCapitalization.words,
                    )),
                    SizedBox(height: 10.0,),
                    Obx(() => FormBuilderTextField(
                      keyboardType: TextInputType.text,
                      obscureText: _showNewPassword.value,
                      attribute: 'contraseniaNueva',
                      initialValue: '',
                      decoration: InputDecoration(
                          labelText: 'Escriba su nueva contraseña',
                          suffixIcon: IconButton(
                            icon: _showNewPassword.value? Icon(Ionicons.eye_outline): Icon(Ionicons.eye_off_outline),
                            onPressed: () {
                              _showNewPassword.value = !_showNewPassword.value;
                            },
                          )
                      ),
                      validators: [
                        FormBuilderValidators.required(errorText: 'Este campo es requerido'),
                      ],
                      textCapitalization: TextCapitalization.words,
                    )),
                    SizedBox(height: 10.0,),
                    Obx(() => FormBuilderTextField(
                      keyboardType: TextInputType.text,
                      obscureText: _showNewPassword.value,
                      attribute: 'contraseniaRepetida',
                      initialValue: '',
                      decoration: InputDecoration(
                          labelText: 'Repita su nueva contraseña',
                          suffixIcon: IconButton(
                            icon: _showNewPassword.value? Icon(Ionicons.eye_outline): Icon(Ionicons.eye_off_outline),
                            onPressed: () {
                              _showNewPassword.value = !_showNewPassword.value;
                            },
                          )
                      ),
                      validators: [
                        FormBuilderValidators.required(errorText: 'Este campo es requerido'),
                      ],
                      textCapitalization: TextCapitalization.words,
                    )),
                  ],
                ),
              ),
              CustomButton(
                backgroundColor: Color.fromARGB(255, 52, 152, 219),
                borderColor: Color.fromARGB(255, 52, 152, 219),
                text: 'Actualizar contraseña',
                disabled: false,
                tap: _updatePassword,
                height: 20.0,
                width: Get.width * 0.5,
                radius: 10.0,
              ),
            ],
          ),
        )
    );
  }

  void _updateProfile() async {
    if(_formKey.currentState.saveAndValidate()) {
      dynamic value = _formKey.currentState.value;
      Usuario _usuario = Usuario.fromJson(value);
      _usuario.id = _globalController.idUsuario;
      ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Guardando...');
      await pr.show();
      ResponseResult result = await _usuarioProvider.updateProfileData(_usuario, _token);
      await pr.hide();
      if(result.code == ResponseResult.successCode) {
        usuario.id = _usuario.id;
        usuario.nombre = _usuario.nombre;
        usuario.genero = _usuario.genero;
        usuario.telefono = _usuario.telefono;
        usuario.fechaNacimiento = _usuario.fechaNacimiento;
        usuario.contactarPorWhatsapp = _usuario.contactarPorWhatsapp;
        userName.value = usuario.nombre;
        _globalController.setUser(usuario);
        Get.back();
        showToast('Actualización', 'Datos guardados con éxito', CustomToastType.Info);
      } else {
        showToast('Actualización', 'Hubo un error al actualizar tus datos', CustomToastType.Error);
      }
    }
  }

  void _updatePassword() async {
    if(_formKeyPassword.currentState.saveAndValidate()) {
      dynamic value = _formKeyPassword.currentState.value;
      if(value['contraseniaNueva'] == value['contraseniaRepetida']) {
        Usuario _usuario = Usuario(id: _globalController.idUsuario, contrasenia: value['contrasenia'], contraseniaRepetida: value['contraseniaRepetida']);
        ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Guardando...');
        await pr.show();
        ResponseResult result = await _usuarioProvider.changePassword(_usuario, _token);
        await pr.hide();
        if(result.code == ResponseResult.successCode) {
          Get.back();
          showToast('Contraseña', 'Contraseña actualizada con éxito', CustomToastType.Info);
        } else {
          showToast('Contraseña', 'Hubo un error al actualizar tu contraseña', CustomToastType.Error);
        }
      } else {
        showToast('Contraseña', 'Las contraseñas no coinciden', CustomToastType.Warning);
      }
    }
  }
}