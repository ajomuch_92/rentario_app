import 'dart:async';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class SelectPlaceController extends GetxController {
  Completer<GoogleMapController> _controller = Completer();
  CameraPosition initialPoint = CameraPosition(
    target: LatLng(14.5197191,-86.6678454),
    zoom: 10
  );
  List<Marker> markers = <Marker>[];
  LatLng _finalPosition;

  @override
  onInit() {
    super.onInit();
  }

  onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    getLocation(controller);
  }

  Future<void> getLocation(GoogleMapController controller) async{
      Location location = new Location();
      if(Get.arguments == null) {
        LocationData _locationData = await location.getLocation();
        _finalPosition = LatLng(_locationData.latitude, _locationData.longitude);
      } else {
        _finalPosition = LatLng(Get.arguments['lat'], Get.arguments['lng']);
      }
      initialPoint = CameraPosition(
          target: _finalPosition,
          zoom: 14.45
      );
      markers.add(Marker(
        markerId: MarkerId('currentPosition'),
        position:  _finalPosition,
        infoWindow: InfoWindow(title: 'Tu posición',snippet: 'Muevela donde desees'),
        draggable: true,
        onDragEnd: (value) {
          _finalPosition = value;
        },
      ));
      await controller.animateCamera(
        CameraUpdate.newCameraPosition(initialPoint)
      );
      update();
  }

  savePosition() {
    Get.back(result: _finalPosition);
  }
}