import 'package:get/get.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentario_app/data/provider/login_provider.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/usuario.dart';
import 'package:rentario_app/utils/Utils.dart';

class RecoveryController extends GetxController{
  RxInt step = 0.obs;
  LoginProvider _loginProvider = LoginProvider();
  Usuario _usuario = Usuario();
  String _email;
  String _code;
  String _newPassword;
  String _jwt;

  void emailChange(String value) {
    _email = value;
  }

  void codeChange(String value) {
    _code = value.toUpperCase();
  }

  void passwordChange(String value) {
    _newPassword = value;
  }

  void getCode() async {
    if(GetUtils.isEmail(_email)) {
      ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Verificando');
      await pr.show();
      ResponseResult result = await _loginProvider.getCode(_email);
      await pr.hide();
      if(result.code == ResponseResult.successCode) {
        step.value = 1;
        _usuario.codigoRecuperacion = result.result['code'];
        _usuario.correo = _email;
        _jwt = result.result['jwt'];
      } else if(result.code == 404) {
        showToast('Recuperación', 'El correo ingresado no está registrado', CustomToastType.Error);
      } else {
        showToast('Recuperación', 'Hubo un error al tratar de verificar tu correo', CustomToastType.Error);
      }
    } else {
      showToast('Recuperación', 'Debe ingresar una dirección de correo válida', CustomToastType.Warning);
    }
  }

  void verifyCode() {
    if(_code == _usuario.codigoRecuperacion) {
      step.value = 2;
    } else {
      showToast('Recuperación', 'El código ingresado es inválido', CustomToastType.Error);
    }
  }

  void resetPassword() async {
    if(!GetUtils.isNullOrBlank(_newPassword)) {
      _usuario.contrasenia = _newPassword;
      ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Guardando');
      await pr.show();
      ResponseResult result = await _loginProvider.resetPassword(_usuario, _jwt);
      await pr.hide();
      if(result.code == ResponseResult.successCode) {
        Get.back(result: 1);
      }  else {
        showToast('Recuperación', 'Hubo un error al guardar tu nueva contraseña', CustomToastType.Error);
      }
    } else {
      showToast('Recuperación', 'Debe ingresar una contraseña', CustomToastType.Warning);
    }
  }
}