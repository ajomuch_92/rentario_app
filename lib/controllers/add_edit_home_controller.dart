import 'package:flutter/cupertino.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:rentario_app/views/select_place.dart';
import 'package:rentario_app/data/provider/miscellaneous_provider.dart';
import 'package:rentario_app/models/tipo_vivienda.dart';
import 'package:rentario_app/models/pais.dart';
import 'package:rentario_app/models/division_pais.dart';
import 'package:rentario_app/models/subdivision_pais.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/foto.dart';
import 'package:rentario_app/models/casa.dart';
import 'package:image_picker_gallery_camera/image_picker_gallery_camera.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as Path;
import 'package:rentario_app/data/provider/casa_provider.dart';
import 'package:rentario_app/controllers/global_controller.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentario_app/utils/Utils.dart';
import 'package:location/location.dart';
import 'package:rentario_app/views/lista_comentarios.dart';

class AddEditHomeController extends GetxController {
  final GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  final CasaProvider _casaProvider = CasaProvider();
  final GlobalController _globalController = Get.find();
  int idCasa;
  Casa currentCasa;
  MiscellaneousProvider _miscellaneousProvider = MiscellaneousProvider();
  List<TipoVivienda> tiposViviendas = <TipoVivienda>[];
  List<Pais> paises = <Pais>[];
  List<DivisionPais> listaDivision = <DivisionPais>[];
  List<SubdivisionPais> listaSubDivision = <SubdivisionPais>[];
  List<Foto> fotos = [];
  RxString coordenadas = ''.obs;
  LatLng _position;
  int division;
  int subdivision;
  Location _location = new Location();

  @override
  onInit() {
    super.onInit();
    if(Get.arguments != null)
      idCasa = Get.arguments['idCasa'];
  }

  @override
  onReady() {
    super.onReady();
    _getLocation();
  }

  Future<int> loadData() async{
    ResponseResult result = await _miscellaneousProvider.initAddEditPage();
    if(result.code == ResponseResult.successCode) {
      tiposViviendas = result.result['tipoViviendas'];
      paises = result.result['paises'];
      if(idCasa != null) {
        ResponseResult result3 = await _casaProvider.getHouse(idCasa, _globalController.token);
        if(result3.code == ResponseResult.successCode) {
          currentCasa = result3.result;
          ResponseResult result2 = await _miscellaneousProvider.getCompletePlaceTree(currentCasa.idSubdivision);
          if(result2.code == ResponseResult.successCode) {
            currentCasa.idPais = result2.result['idPais'];
            currentCasa.idDivision = result2.result['idDivision'];
            paises = result2.result['paises'];
            listaDivision = result2.result['division'];
            listaSubDivision = result2.result['subdivision'];
            coordenadas.value = '${currentCasa.latitud}, ${currentCasa.longitud}';
            fotos = currentCasa.fotos;
          }
        }
      }
      return 1;
    }
    return 0;
  }

  void onChangePais(dynamic idPais) async{
    ResponseResult result = await _miscellaneousProvider.getDivision(idPais);
    if(result.code == ResponseResult.successCode) {
      listaDivision = result.result;
      if(currentCasa != null)
        currentCasa.idDivision = null;
      update(['division']);
    }
  }

  void onChangeDivision(dynamic idDivision) async{
    ResponseResult result = await _miscellaneousProvider.getSubDivision(idDivision);
    if(result.code == ResponseResult.successCode) {
      listaSubDivision = result.result;
      if(currentCasa != null)
        currentCasa.idSubdivision = null;
      update(['subdivision']);
    }
  }

  void _getLocation() async {
    bool _serviceEnabled = await _location.serviceEnabled();
    if(!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
    }
    if(!_serviceEnabled)
      showToast('Ubicación', 'Debes encender tu ubicación para acceder al mapa', CustomToastType.Warning);
  }

  goToMap() async{
    bool _serviceEnabled = await _location.serviceEnabled();
    if(!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
    }
    if(_serviceEnabled) {
      dynamic value;
      if(idCasa != null) {
        value = await Get.to(SelectPlace(), arguments: {'lat': currentCasa.latitud, 'lng': currentCasa.longitud});
      } else {
        value = await Get.to(SelectPlace());
      }
      if(value != null) {
        _position = value as LatLng;
        coordenadas.value = '${_position.latitude}, ${_position.longitude}';
      }
    }
  }

  Future<void> getImage() async {
    var image = await ImagePickerGC.pickImage(
      context: Get.overlayContext,
      source: ImgSource.Both,
      imageQuality: 50,
      galleryText: Text('Galería'),
      cameraText: Text('Cámara')
    );
    if (image != null) {
      Foto _foto = Foto(file: image);
      fotos.add(_foto);
      update(['fotos']);
    }
  }
  
  void deleteImage(int index) async{
    if(idCasa != null) {
      ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Eliminando...');
      Foto _foto = fotos[index];
      await pr.show();
      Reference storageReference = await FirebaseStorage.instance.refFromURL(_foto.url);
      await storageReference.delete();
      await pr.hide();
    }
    fotos.removeAt(index);
    update(['fotos']);
  }

  void saveHome(){
    if(idCasa !=null) {
      _editHome();
    } else {
      _addNewHome();
    }
  }

  void _addNewHome() async {
    if(formKey.currentState.saveAndValidate()) {
      Map<String, dynamic> values = formKey.currentState.value;
      Casa casa = Casa.fromJson(values);
      casa.estado = true;
      casa.fechaPublicacion = DateTime.now();
      casa.idUsuario = _globalController.idUsuario;
      if(_position != null) {
        casa.latitud = _position.latitude;
        casa.longitud = _position.longitude;
      }
      ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Guardando...');
      await pr.show();
      for(Foto foto in fotos) {
        Reference storageReference = FirebaseStorage.instance
            .ref()
            .child('houses/${Path.basename(foto.file.path)}');
        UploadTask uploadTask = storageReference.putFile(foto.file);
        await Future.value(uploadTask);
        String _url = await storageReference.getDownloadURL();
        foto.url = _url;
      }
      casa.fotos = fotos;
      ResponseResult result = await _casaProvider.saveHome(casa, _globalController.token);
      await pr.hide();
      if (result.code == ResponseResult.successCode) {
        fotos = [];
        listaDivision = [];
        listaSubDivision = [];
        coordenadas.value = '';
        formKey.currentState.reset();
        update(['division', 'subdivision', 'fotos']);
        showToast('Guardar', 'Se ha guardado y publicado su casa con éxito', CustomToastType.Info);
      } else {
        showToast('Guardar', 'Hubo un error al publicar su casa', CustomToastType.Error);
      }
    }
  }

  void _editHome() async {
    if(formKey.currentState.saveAndValidate()) {
      Map<String, dynamic> values = formKey.currentState.value;
      Casa casa = Casa.fromJson(values);
      casa.id = idCasa;
      casa.idUsuario = _globalController.idUsuario;
      casa.fechaPublicacion = currentCasa.fechaPublicacion;
      if(_position != null && _position.latitude != currentCasa.latitud && _position.longitude != currentCasa.longitud) {
        casa.latitud = _position.latitude;
        casa.longitud = _position.longitude;
      } else {
        casa.latitud = currentCasa.latitud;
        casa.longitud = currentCasa.longitud;
      }
      ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Guardando...');
      await pr.show();
      for(Foto foto in fotos) {
        if(GetUtils.isNullOrBlank(foto.url)) {
          Reference storageReference = FirebaseStorage.instance
              .ref()
              .child('houses/${Path.basename(foto.file.path)}');
          UploadTask uploadTask = storageReference.putFile(foto.file);
          await Future.value(uploadTask);
          String _url = await storageReference.getDownloadURL();
          foto.url = _url;
        }
      }
      casa.fotos = fotos;
      ResponseResult result = await _casaProvider.saveChangesHome(casa, _globalController.token);
      await pr.hide();
      if (result.code == ResponseResult.successCode) {
        Get.back(result: casa);
      } else {
        showToast('Guardar', 'Hubo un error al guardar tus cambios', CustomToastType.Error);
      }
    }
  }

  void goToComments() {
    Get.to(ListaComentarios(), arguments: {'id': idCasa});
  }
}