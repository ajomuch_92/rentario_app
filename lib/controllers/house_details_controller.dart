import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';
import 'package:rentario_app/controllers/global_controller.dart';
import 'package:rentario_app/data/provider/appointment_notification_provider.dart';
import 'package:rentario_app/data/provider/puntuacion_provider.dart';
import 'package:rentario_app/models/casa.dart';
import 'package:rentario_app/models/cita.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/views/lista_comentarios.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/material.dart';
import 'package:rentario_app/models/puntuacion.dart';
import 'package:rentario_app/models/usuario.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:rentario_app/utils/Utils.dart';
import 'package:progress_dialog/progress_dialog.dart';

class HouseDetailsController extends GetxController {
  GlobalController _globalController = Get.find();
  PuntuacionProvider _puntuacionProvider = PuntuacionProvider();
  AppointmentNotificationProvider _appointmentNotificationProvider = AppointmentNotificationProvider();
  Usuario usuario;
  Casa casa = Casa();

  @override
  void onInit() {
    super.onInit();
    casa = Get.arguments['casa'] as Casa;
    usuario = _globalController.usuario;
  }

  void openMap() async {
    var uri = Uri.parse("https://www.google.com/maps/search/?api=1&query=${casa.latitud},${casa.longitud}");
    if (await canLaunch(uri.toString())) {
      await launch(uri.toString());
    }
  }

  void showProfile(){
    Get.dialog(AlertDialog(
      content: Container(
        height: 210.0,
        child: Column(
          children: [
            CircleAvatar(
              backgroundImage: GetUtils.isNullOrBlank(casa.urlPerfil)?
              AssetImage('assets/images/man.png'):
              NetworkImage(casa.urlPerfil),
              radius: 70.0,
            ),
            Center(
              child: Text(casa.arrendatario),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                !GetUtils.isNullOrBlank(casa.contactarPorWhatsapp) && casa.contactarPorWhatsapp?
                IconButton(
                    color: Colors.green,
                    icon: Icon(Ionicons.logo_whatsapp),
                    onPressed: _sendMessageByWhatsapp
                ): Container(),
                IconButton(
                    color: Colors.orangeAccent,
                    icon: Icon(Ionicons.call_outline),
                    onPressed: _makePhoneCall
                )
              ],
            ),
          ],
        ),
      ),
    ));
  }

  void _makePhoneCall() async{
    String phone = 'tel:${casa.telefono}';
    if (await canLaunch(phone)) {
      await launch(phone);
    }
  }

  void _sendMessageByWhatsapp() async{
    String phone = 'https://wa.me/504${casa.telefono}';
    if (await canLaunch(phone)) {
      await launch(phone);
    }
  }

  void goToComments() {
    Get.to(ListaComentarios(), arguments: {'id': casa.id});
  }

  void showCommentModal() {
    Puntuacion _puntuacion = Puntuacion(idUsuario: _globalController.idUsuario, idVivienda: casa.id);
    Get.dialog(AlertDialog(
      title: Text('Califica esta casa'),
      content: Container(
        height: 180.0,
        width: Get.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              textInputAction: TextInputAction.done,
              decoration: InputDecoration(
                labelText: 'Comentario',
                suffixIcon: Icon(LineAwesomeIcons.comment),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black12)
                )
              ),
              maxLines: 3,
              maxLength: 250,
              onChanged: (value) {
                _puntuacion.comentario = value;
              },
            ),
            Text('Puntuación'),
            RatingBar.builder(
              initialRating: 0,
              itemSize: 20.0,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: false,
              itemCount: 5,
              itemBuilder: (context, _) => Icon(
                Ionicons.star,
                color: Colors.amber,
              ),
              ignoreGestures: false,
              onRatingUpdate: (value) {
                _puntuacion.puntuacion = value.toInt();
              },
            ),
          ],
        ),
      ),
      actions: [
        new FlatButton(
          child: new Text('Calificar'),
          onPressed: () {
            _savePuntuacion(_puntuacion);
          },
        ),
        new FlatButton(
          child: new Text('Cancelar'),
          onPressed: () {
            Get.back();
          },
        ),
      ],
    ));
  }

  void showAppointmentModal() {
    final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();
    Get.dialog(AlertDialog(
      title: Text('Haz una cita con el arrendatario para ver este lugar'),
      content: Container(
        height: 200.0,
        width: Get.width * 0.9,
        child: FormBuilder(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                FormBuilderTextField(
                  minLines: 3,
                  maxLines: 5,
                  attribute: 'comentario',
                  initialValue: '',
                  textInputAction: TextInputAction.next,
                  maxLength: 250,
                  decoration: InputDecoration(
                    labelText: 'Comentario',
                    prefixIcon: Icon(LineAwesomeIcons.digital_tachograph),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black12)
                    )
                  ),
                ),
                FormBuilderDateTimePicker(
                  attribute: 'fechaCita',
                  inputType: InputType.both,
                  format: DateFormat('dd/MM/yyyy hh:mm a'),
                  decoration: InputDecoration(
                    labelText: 'Fecha/Hora de la cita',
                    suffixIcon: Icon(Ionicons.calendar_outline),
                  ),
                  cancelText: 'Cancelar',
                  confirmText: 'Aceptar',
                  validators: [
                    FormBuilderValidators.required(errorText: 'Este campo es requerido')
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      actions: [
        new FlatButton(
          child: new Text('Enviar'),
          onPressed: () {
            if(_formKey.currentState.saveAndValidate()) {
              dynamic value = _formKey.currentState.value;
              Cita cita = Cita.fromJson(value);
              _saveCita(cita);
            }
          },
        ),
        new FlatButton(
          child: new Text('Cancelar'),
          onPressed: () {
            Get.back();
          },
        ),
      ],
    ));
  }

  void _savePuntuacion(Puntuacion puntuacion) async {
    if(GetUtils.isNull(puntuacion.puntuacion) || GetUtils.isNull(puntuacion.comentario)) {
      showToast('Calificación', 'Debes ingresar la puntuación y el comentario para continuar', CustomToastType.Warning);
    } else {
      ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Guardando');
      await pr.show();
      puntuacion.fecha = DateTime.now();
      ResponseResult result = await _puntuacionProvider.savePuntuacion(puntuacion, _globalController.token);
      await pr.hide();
      if(result.code == ResponseResult.successCode) {
        Get.back();
        showToast('Calificación', 'Tu calificación fue publicada con éxito', CustomToastType.Info);
      } else {
        showToast('Calificación', 'Hubo un error al publicar tu calificación', CustomToastType.Error);
      }
    }
  }

  void _saveCita(Cita cita) async {
    cita.fechaCreacion = DateTime.now();
    cita.idCliente = _globalController.idUsuario;
    cita.idVivienda = casa.id;
    ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Guardando');
    await pr.show();
    ResponseResult result = await _appointmentNotificationProvider.saveAppointment(cita, _globalController.token);
    await pr.hide();
    if(result.code == ResponseResult.successCode) {
      Get.back();
      showToast('Cita', 'Tu calificación fue guardada con éxito, espera su confirmación', CustomToastType.Info);
    } else {
      showToast('Cita', 'Hubo un error al guardar tu cita', CustomToastType.Error);
    }
  }
}