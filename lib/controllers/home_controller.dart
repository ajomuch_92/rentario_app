import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:rentario_app/controllers/global_controller.dart';
import 'package:rentario_app/data/provider/casa_provider.dart';
import 'package:rentario_app/data/provider/favoritas_provider.dart';
import 'package:rentario_app/models/casa.dart';
import 'package:rentario_app/models/filter.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/usuario.dart';
import 'package:rentario_app/views/index.dart';
import 'package:rentario_app/views/notifications.dart';
import 'package:uuid/uuid.dart';
import 'package:rentario_app/views/add_edit_home.dart';
import 'package:rentario_app/utils/Utils.dart';
import 'package:rentario_app/views/filters_advanced.dart';

class HomeController extends GetxController {
  PageController pageController = PageController(initialPage: 0, keepPage: false);
  GlobalController _globalController = Get.find();
  CasaProvider _casaProvider = CasaProvider();
  FavoritasProvider _favoritasProvider = FavoritasProvider();
  Usuario usuario;
  RxInt activePage = 0.obs;
  List<Casa> _listaCasasPrincipal = <Casa>[];
  List<Casa> _listaMisCasas = <Casa>[];
  List<Casa> _listaFavoritas = <Casa>[];
  String _token = '';
  Uuid uuid = Uuid();
  int _size = 5;
  Filter _filter;
  final PagingController<int, Casa> pagingController =  PagingController(firstPageKey: 1);
  final PagingController<int, Casa> pagingOwnHouseController =  PagingController(firstPageKey: 1);
  final PagingController<int, Casa> favoriteHouseController =  PagingController(firstPageKey: 1);

  @override
  void onInit() {
    super.onInit();
    usuario = _globalController.usuario;
    _token = _globalController.token;
    pagingController.addPageRequestListener((pageKey) {
      _loadCasaPrincipales(pageKey);
    });
    if(usuario.tipoUsuario == 1)
      pagingOwnHouseController.addPageRequestListener((pageKey) {
        _loadMisCasas(pageKey);
      });
    else
      favoriteHouseController.addPageRequestListener((pageKey) {
        _loadMisFavoritas(pageKey);
      });
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose(){
    super.onClose();
    pagingController.dispose();
    pagingOwnHouseController.dispose();
  }

  void logout() {
    _globalController.deleteUser();
    Get.off(Index());
  }

  Future<void> _loadCasaPrincipales(int page) async {
    ResponseResult result = await _casaProvider.getHousesByPage(_globalController.idUsuario, _size, page, _token, _filter);
    if(result.code == ResponseResult.successCode) {
      List<Casa> _casas = result.result;
      _listaCasasPrincipal.addAll(_casas);
      bool _isLastPages = _casas.length < _size;
      if(_isLastPages){
        pagingController.appendLastPage(_casas);
      } else{
        pagingController.appendPage(_casas, page + 1);
      }
    } else {
      pagingController.error = result.message;
    }
  }

  Future<void> _loadMisCasas(int page) async {
    ResponseResult result = await _casaProvider.getHousesByUser(_size, page, _globalController.idUsuario, _token);
    if(result.code == ResponseResult.successCode) {
      List<Casa> _casas = result.result;
      _listaMisCasas.addAll(_casas);
      bool _isLastPages = _casas.length < _size;
      if(_isLastPages){
        pagingOwnHouseController.appendLastPage(_casas);
      } else{
        pagingOwnHouseController.appendPage(_casas, page + 1);
      }
    } else {
      pagingOwnHouseController.error = result.message;
    }
  }

  Future<void> _loadMisFavoritas(int page) async {
    ResponseResult result = await _favoritasProvider.getMyFavoritas(_globalController.idUsuario, page, _size, _token);
    if(result.code == ResponseResult.successCode) {
      List<Casa> _casas = result.result;
      _listaFavoritas.addAll(_casas);
      bool _isLastPages = _casas.length < _size;
      if(_isLastPages){
        favoriteHouseController.appendLastPage(_casas);
      } else{
        favoriteHouseController.appendPage(_casas, page + 1);
      }
    } else {
      favoriteHouseController.error = result.message;
    }
  }

  void editHome(id) {
    Get.to(AddEditHome(), arguments: {'idCasa': id}).then((value) {
      if(value != null && value is Casa) {
        int indexMain = _listaMisCasas.indexWhere((element) => element.id == id);
        _listaMisCasas[indexMain].idTipoVivienda = value.idTipoVivienda;
        _listaMisCasas[indexMain].idSubdivision = value.idSubdivision;
        _listaMisCasas[indexMain].colonia = value.colonia;
        _listaMisCasas[indexMain].latitud = value.latitud;
        _listaMisCasas[indexMain].longitud = value.longitud;
        _listaMisCasas[indexMain].descripcion = value.descripcion;
        _listaMisCasas[indexMain].dormitorios = value.dormitorios;
        _listaMisCasas[indexMain].precio = value.precio;
        _listaMisCasas[indexMain].fotos = value.fotos;
        _listaMisCasas[indexMain].estado = value.estado;
        pagingOwnHouseController.itemList = _listaCasasPrincipal;
        int indexGlobal = _listaCasasPrincipal.indexWhere((element) => element.id == id);
        if(indexGlobal != -1) {
          _listaCasasPrincipal[indexGlobal].idTipoVivienda = value.idTipoVivienda;
          _listaCasasPrincipal[indexGlobal].idSubdivision = value.idSubdivision;
          _listaCasasPrincipal[indexGlobal].colonia = value.colonia;
          _listaCasasPrincipal[indexGlobal].latitud = value.latitud;
          _listaCasasPrincipal[indexGlobal].longitud = value.longitud;
          _listaCasasPrincipal[indexGlobal].descripcion = value.descripcion;
          _listaCasasPrincipal[indexGlobal].dormitorios = value.dormitorios;
          _listaCasasPrincipal[indexGlobal].precio = value.precio;
          _listaCasasPrincipal[indexGlobal].fotos = value.fotos;
          _listaCasasPrincipal[indexGlobal].estado = value.estado;
          pagingController.itemList = _listaCasasPrincipal;
        }
        showToast('Guardar', 'Cambios guardados con éxito', CustomToastType.Info);
      }
    });
  }

  void addFavorite(Casa casa) async {
    ResponseResult result = await _favoritasProvider.addFavoritas(_globalController.idUsuario, casa.id, _token);
    if(result.code == ResponseResult.successCode) {
      int index = _listaCasasPrincipal.indexOf(casa);
      casa.favorite = true;
      _listaCasasPrincipal[index] = casa;
      _listaFavoritas.add(casa);
      pagingOwnHouseController.itemList = _listaCasasPrincipal;
      favoriteHouseController.itemList = _listaFavoritas;
      update(['main', 'favorites']);
    } else {
      showToast('Favorita', 'Ocurrió un error al establecer tu favorita', CustomToastType.Error);
    }
  }

  void removeFavorite(Casa casa) async {
    ResponseResult result = await _favoritasProvider.removeFavoritas(_globalController.idUsuario, casa.id, _token);
    if(result.code == ResponseResult.successCode) {
      int indexMain = _listaCasasPrincipal.indexWhere((element) => element.id == casa.id);
      int indexFavorite = _listaFavoritas.indexWhere((element) => element.id == casa.id);
      casa.favorite = false;
      if(indexMain != -1) {
        _listaCasasPrincipal[indexMain].favorite = false;
        pagingOwnHouseController.itemList = _listaCasasPrincipal;
      }
      if(indexFavorite != -1) {
        _listaFavoritas.remove(_listaFavoritas[indexFavorite]);
        favoriteHouseController.itemList = _listaFavoritas;
      }
      update(['main', 'favorites']);
    } else {
      showToast('Favorita', 'Ocurrió un error al borrar tu favorita', CustomToastType.Error);
    }
  }

  void goToNotifications() {
    Get.to(Notifications());
  }

  void searchChange(String value) {
    List<Casa> _listTemp;
    if(!GetUtils.isNullOrBlank(value)) {
      value = value.toLowerCase();
      _listTemp = _listaCasasPrincipal.where((element) => element.descripcion.toLowerCase().contains(value) || element.colonia.toLowerCase().contains(value)).toList();
    } else {
      _listTemp = _listaCasasPrincipal;
    }
    pagingController.itemList = _listTemp;
  }

  void goToFilters() {
    Get.to(FiltersAdvanced(), arguments: {'filter': _filter}).then((value) {
      if(value is Filter) {
        Filter filter = value;
        if(filter.isEmpty()) {
          _filter = null;
        } else {
          _filter = filter;
        }
        pagingController.itemList = [];
        _loadCasaPrincipales(1);
      }
    });
  }

}