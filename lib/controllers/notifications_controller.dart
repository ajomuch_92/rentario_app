import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:rentario_app/controllers/global_controller.dart';
import 'package:rentario_app/data/provider/appointment_notification_provider.dart';
import 'package:rentario_app/data/provider/miscellaneous_provider.dart';
import 'package:rentario_app/models/cita.dart';
import 'package:rentario_app/models/estado_cita.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/usuario.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentario_app/utils/Utils.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:ionicons/ionicons.dart';

class NotificationsController extends GetxController {
  GlobalController _globalController = Get.find();
  AppointmentNotificationProvider _appointmentNotificationProvider = AppointmentNotificationProvider();
  Usuario usuario;
  final PagingController<int, Cita> pagingController =  PagingController(firstPageKey: 0);
  int _size = 10;
  String _token;
  List<Cita> _listCitas = [];
  List<EstadoCita> _listEstados = [];
  List<EstadoCita> _listEstadosGlobal = [];

  @override
  void onInit() {
    super.onInit();
    usuario = _globalController.usuario;
    _token = _globalController.token;
    _getEstados();
    pagingController.addPageRequestListener((pageKey) {
      _loadCitas(pageKey);
    });
  }

  void _getEstados() {
    MiscellaneousProvider()..getEstadoCitas().then((value){
      if(value.code == ResponseResult.successCode) {
        _listEstados = value.result;
        _listEstadosGlobal = value.result;
        if(usuario.tipoUsuario == 1) {
          _listEstados = _listEstados.where((element) => element.accion.toLowerCase() != 'enviar').toList();
        } else {
          List<String> acciones = ['enviar', 'rechazar', 'aceptar'];
          _listEstados = _listEstados.where((element) => !acciones.contains(element.accion.toLowerCase())).toList();
        }
      }
    });
  }

  void _loadCitas(int page) async{
    ResponseResult result;
    if(usuario.tipoUsuario == 1) {
      result = await _appointmentNotificationProvider.getAppointments(_globalController.idUsuario, page, _size, _token);
    } else {
      result = await _appointmentNotificationProvider.getAppointmentsByClient(_globalController.idUsuario, page, _size, _token);
    }
    if(result.code == ResponseResult.successCode) {
      List<Cita> citas = result.result;
      _listCitas.addAll(citas);
      bool _isLastPages = citas.length < _size;
      if(_isLastPages){
        pagingController.appendLastPage(citas);
      } else{
        pagingController.appendPage(citas, page + 1);
      }
    } else {
      pagingController.error = result.message;
    }
  }

  List<PopupMenuItem<EstadoCita>> getMenuOptions() {
    List<PopupMenuItem<EstadoCita>> options = _listEstados.map((e) => PopupMenuItem<EstadoCita>(
      value: e,
      child: Text(e.accion),
    )).toList();
    return options;
  }

  void onMenuSelected(Cita cita, EstadoCita estadoSelected) {
    if(estadoSelected.id != 4) {
      Get.dialog(AlertDialog(
        title: Text('Actualizar cita'),
        content: Text('¿Estás seguro que deseas ${estadoSelected.accion.toLowerCase()} esta cita?'),
        actions: [
          new FlatButton(
            child: new Text('Cambiar'),
            onPressed: () {
              cita.estado = estadoSelected.id;
              cita.estadoCita = estadoSelected.estado;
              _updateCita(cita);
            },
          ),
          new FlatButton(
            child: new Text('Cancelar'),
            onPressed: () {
              Get.back();
            },
          ),
        ],
      ));
    } else {
      final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();
      Get.dialog(AlertDialog(
        title: Text('Actualizar cita'),
        content: Container(
          width: Get.width * 0.9,
          child: FormBuilder(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  FormBuilderDateTimePicker(
                    attribute: 'fechaCita',
                    inputType: InputType.both,
                    format: DateFormat('dd/MM/yyyy hh:mm a'),
                    decoration: InputDecoration(
                      labelText: 'Fecha/Hora nueva de la cita',
                      suffixIcon: Icon(Ionicons.calendar_outline),
                    ),
                    cancelText: 'Cancelar',
                    confirmText: 'Aceptar',
                    validators: [
                      FormBuilderValidators.required(errorText: 'Este campo es requerido')
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        actions: [
          new FlatButton(
            child: new Text('Cambiar'),
            onPressed: () {
              if(_formKey.currentState.saveAndValidate()) {
                Map<String, dynamic> value = _formKey.currentState.value;
                cita.estado = estadoSelected.id;
                cita.estadoCita = estadoSelected.estado;
                cita.fechaCita = value['fechaCita'];
                _updateCita(cita, updateFecha: true);
              }
            },
          ),
          new FlatButton(
            child: new Text('Cancelar'),
            onPressed: () {
              Get.back();
            },
          ),
        ],
      ));
    }
  }

  void _updateCita(Cita cita, {bool updateFecha}) async {
    Cita newCita = Cita(id: cita.id, estado: cita.estado, arrendatario: _globalController.usuario.tipoUsuario == 1);
    if(updateFecha != null && updateFecha == true) {
      newCita.fechaCita = cita.fechaCita;
    }
    ProgressDialog pr = getLoadingDialog(context: Get.overlayContext, text: 'Guardando');
    await pr.show();
    ResponseResult result = await _appointmentNotificationProvider.updateAppointment(newCita, _globalController.token);
    await pr.hide();
    if(result.code == ResponseResult.successCode) {
      Get.back();
      pagingController.refresh();
      showToast('Cita', 'Tu cita fue actualizada con éxito', CustomToastType.Info);
    } else {
      showToast('Cita', 'Hubo un error al actualizar tu cita', CustomToastType.Error);
    }
  }

  void showProfile(Usuario usuario){
    Get.dialog(AlertDialog(
      content: Container(
        height: 210.0,
        child: Column(
          children: [
            CircleAvatar(
              backgroundImage: GetUtils.isNullOrBlank(usuario.urlPerfil)?
              AssetImage('assets/images/man.png'):
              NetworkImage(usuario.urlPerfil),
              radius: 70.0,
            ),
            Center(
              child: Text(usuario.nombre),
            ),
            Center(
              child: IconButton(
                  color: Colors.orangeAccent,
                  icon: Icon(Ionicons.call_outline),
                  onPressed: () {
                    _makePhoneCall(usuario.telefono);
                  }
              ),
            ),
          ],
        ),
      ),
    ));
  }

  void _makePhoneCall(String telefono) async{
    String phone = 'tel:$telefono';
    if (await canLaunch(phone)) {
      await launch(phone);
    }
  }
}