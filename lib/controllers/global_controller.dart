import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:rentario_app/models/usuario.dart';

class GlobalController extends GetxController {
  GetStorage _storage = GetStorage('rentario');
  String fcmToken = '';
  GetStorage get storage => _storage;

  void setUser(Usuario usuario) {
    dynamic jsonUsuario = usuario.toCompleteJson();
    _storage.write('usuario', jsonUsuario);
    if(!GetUtils.isNullOrBlank(usuario.token))
      _storage.write('token', usuario.token);
    if(!GetUtils.isNullOrBlank(usuario.id.toString()))
      _storage.write('idUsuario', usuario.id);
  }

  void deleteUser(){
    _storage.remove('usuario');
    _storage.remove('token');
    _storage.remove('idUsuario');
  }

  Usuario get usuario {
    dynamic _usuario = _storage.read('usuario');
    return _usuario == null? null: Usuario.fromJson(_usuario);
  }

  String get token => _storage.read<String>('token');

  int get idUsuario => _storage.read<int>('idUsuario');

  void setDev(bool isDev) {
    _storage.write('isDev', isDev);
  }

  bool get dev => _storage.read<bool>('isDev');
}