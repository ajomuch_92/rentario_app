import 'package:get/get.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:rentario_app/data/provider/miscellaneous_provider.dart';
import 'package:rentario_app/models/tipo_vivienda.dart';
import 'package:rentario_app/models/pais.dart';
import 'package:rentario_app/models/division_pais.dart';
import 'package:rentario_app/models/subdivision_pais.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/filter.dart';

class FilterAdvancedController extends GetxController {
  final GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  MiscellaneousProvider _miscellaneousProvider = MiscellaneousProvider();
  RxList<TipoVivienda> tiposViviendas = RxList(<TipoVivienda>[]);
  RxList<Pais> paises = RxList(<Pais>[]);
  RxList<DivisionPais> listaDivision = RxList(<DivisionPais>[]);
  RxList<SubdivisionPais> listaSubDivision = RxList(<SubdivisionPais>[]);
  Filter filter;

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments != null && Get.arguments['filter'] != null)
      filter = Get.arguments['filter'];
    else
      filter = Filter();
    loadData();
  }

  Future<void> loadData() async{
    ResponseResult result;
    if(filter.isEmpty()) {
      result = await _miscellaneousProvider.initAddEditPage();
      if(result.code == ResponseResult.successCode) {
        tiposViviendas.assignAll(result.result['tipoViviendas']);
        paises.assignAll(result.result['paises']);
      }
    } else {
      result = await _miscellaneousProvider.getCompletePlaceTree(filter.idSubdivision);
      if(result.code == ResponseResult.successCode) {
        paises.assignAll(result.result['paises']);
        listaDivision.assignAll(result.result['division']);
        listaSubDivision.assignAll(result.result['subdivision']);
      }
    }
  }


  void onChangePais(dynamic idPais) async{
    ResponseResult result = await _miscellaneousProvider.getDivision(idPais);
    if(result.code == ResponseResult.successCode) {
      formKey.currentState.resetField('idDivision');
      formKey.currentState.resetField('idSubdivision');
      listaDivision.assignAll(result.result);
    }
  }

  void onChangeDivision(dynamic idDivision) async{
    ResponseResult result = await _miscellaneousProvider.getSubDivision(idDivision);
    if(result.code == ResponseResult.successCode) {
      formKey.currentState.resetField('idSubdivision');
      listaSubDivision.assignAll(result.result);
    }
  }

  void resetFilter() {
    formKey.currentState.reset();
    listaDivision.assignAll([]);
    listaSubDivision.assignAll([]);
    filter = Filter();
    Get.back(result: filter);
  }

  void applyFilter() {
    formKey.currentState.save();
    dynamic values = formKey.currentState.value;
    filter = Filter.fromCustomJson(values);
    Get.back(result: filter);
  }

}