import 'package:flutter/cupertino.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentario_app/data/provider/login_provider.dart';
import 'package:rentario_app/data/provider/miscellaneous_provider.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/tipo_usuario.dart';
import 'package:rentario_app/models/usuario.dart';
import 'package:rentario_app/utils/Utils.dart';

class RegisterController extends GetxController {
  Widget separator = SizedBox(height: 5.0,);
  final GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  MiscellaneousProvider _miscellaneousProvider = MiscellaneousProvider();
  LoginProvider _loginProvider = LoginProvider();

  RxList<TipoUsuario> tiposUsuarios = RxList();

  @override
  void onInit() {
    super.onInit();
    _loadTiposUsuarios();
  }

  void back() {
    Get.back();
  }

  _loadTiposUsuarios() async{
    ResponseResult result = await _miscellaneousProvider.getTipoUsuarios();
    if(result.code == ResponseResult.successCode) {
      tiposUsuarios.value = result.result as List<TipoUsuario>;
    }
  }

  login() async {
    if(formKey.currentState.saveAndValidate()) {
      Map<String, dynamic> values = formKey.currentState.value;
      Usuario _usuario = Usuario.fromJson(values);
      ProgressDialog _pr = getLoadingDialog(context: Get.overlayContext, text: 'Creating');
      await _pr.show();
      ResponseResult _result = await _loginProvider.createUser(_usuario);
      await _pr.hide();
      if(_result.code == ResponseResult.successCode){
        showToast('Cración', 'Usuario creado con éxito', CustomToastType.Info);
        formKey.currentState.reset();
      } else if(_result.code == 400) {
        showToast('Cración', 'Hubo un error al crear tu usuario', CustomToastType.Error);
      } else {
        showToast('Cración', 'Hubo un error al crear tu usuario', CustomToastType.Error);
      }
    }
  }
}