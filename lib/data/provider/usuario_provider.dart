import 'package:dio/dio.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/usuario.dart';
import 'dart:io';
import 'adapter.dart';

class UsuarioProvider {
  Dio _dio = Adapter.getAdapter();
  ResponseResult _result = ResponseResult();

  Future<ResponseResult> updateProfilePicture(Usuario usuario, String token) async{
    try{
      List<Map<String, dynamic>> data = [
        {
          'op': 'replace',
          'path': '/UrlPerfil',
          'value': usuario.urlPerfil
        }
      ];
      await _dio.patch('Usuarios/${usuario.id}',
          data: data,
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ));
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> updateProfileData(Usuario usuario, String token) async{
    try{
      List<Map<String, dynamic>> data = [
        {
          'op': 'replace',
          'path': '/ContactarPorWhatsapp',
          'value': usuario.contactarPorWhatsapp??false
        }
      ];
      if(usuario.fechaNacimiento != null) {
        data.add({
          'op': 'replace',
          'path': '/FechaNacimiento',
          'value': usuario.fechaNacimiento.toString()
        });
      }
      if(usuario.genero != null) {
        data.add({
          'op': 'replace',
          'path': '/Genero',
          'value': usuario.genero
        });
      }
      if(usuario.nombre != null) {
        data.add({
          'op': 'replace',
          'path': '/Nombre',
          'value': usuario.nombre
        });
      }
      if(usuario.telefono != null) {
        data.add({
          'op': 'replace',
          'path': '/Telefono',
          'value': usuario.telefono
        });
      }
      if(usuario.telefono != null) {
        data.add({
          'op': 'replace',
          'path': '/Telefono',
          'value': usuario.telefono
        });
      }
      if(usuario.fcmToken != null) {
        data.add({
          'op': 'replace',
          'path': '/PrimeraVez',
          'value': false
        });
        data.add({
          'op': 'replace',
          'path': '/FcmToken',
          'value': usuario.fcmToken
        });
      }
      await _dio.patch('Usuarios/${usuario.id}',
          data: data,
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ));
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getCalification(int idUsuario, String token) async{
    try{
      Response response = await _dio.get('Usuarios/GetMyCalification',
          queryParameters: {'idUsuario': idUsuario},
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ));
      _result.code = ResponseResult.successCode;
      _result.result = response.data;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> changePassword(Usuario usuario, String token) async{
    try{
      await _dio.post('Usuarios/UpdatePassword',
          data: usuario.toJsonUpdatePass(),
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ));
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }
}