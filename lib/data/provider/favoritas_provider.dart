import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/casa.dart';
import 'dart:io';
import 'adapter.dart';

class FavoritasProvider {
  Dio _dio = Adapter.getAdapter();
  ResponseResult _result = ResponseResult();

  Future<ResponseResult> getMyFavoritas(int idUsuario, int page, int size, String token) async{
    try{
      Response response = await _dio.get('Favoritas/GetMyFavoritas/${idUsuario}',
        queryParameters: {
          'page': page,
          'size': size
        },
        options: Options(
            responseType: ResponseType.json,
            headers: {
              HttpHeaders.authorizationHeader: 'Bearer $token',
              HttpHeaders.contentTypeHeader: 'application/json'
            }
        )
      );
      dynamic data = response.data;
      List<Casa> casas = [];
      for(Map i in data) {
        casas.add(Casa.fromCustomJson(i));
      }
      _result.result = casas;
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> addFavoritas(int idUsuario, int idVivienda, String token) async{
    try{
      await _dio.post('Favoritas',
          data: {
            'idUsuario': idUsuario,
            'idVivienda': idVivienda,
            'fecha': DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now())
          },
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          )
      );
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> removeFavoritas(int idUsuario, int idVivienda, String token) async{
    try{
      await _dio.delete('Favoritas/DeleteFavorita',
          queryParameters: {
            'idUsuario': idUsuario,
            'idVivienda': idVivienda
          },
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          )
      );
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }
}