import 'package:dio/dio.dart';
import 'package:get/utils.dart';
import 'package:rentario_app/data/provider/adapter.dart';
import 'package:rentario_app/models/estado_cita.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:rentario_app/models/tipo_usuario.dart';
import 'package:rentario_app/models/tipo_vivienda.dart';
import 'package:rentario_app/models/pais.dart';
import 'package:rentario_app/models/division_pais.dart';
import 'package:rentario_app/models/subdivision_pais.dart';

class MiscellaneousProvider {
  Dio _dio = Adapter.getAdapter();
  ResponseResult _result = ResponseResult();

  Future<ResponseResult> getTipoUsuarios() async{
    try {
      Response response = await _dio.get('TipoUsuarios');
      if(response.statusCode == 200) {
        dynamic data = response.data;
        List<TipoUsuario> list = [];
        for(Map i in data) {
          list.add(TipoUsuario.fromJson(i));
        }
        _result.result = list;
        _result.code = ResponseResult.successCode;
      } else {
        _result.code = response.statusCode;
        _result.message = response.statusMessage;
      }
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getEstadoCitas() async{
    try {
      Response response = await _dio.get('EstadoCitas');
      if(response.statusCode == 200) {
        dynamic data = response.data;
        List<EstadoCita> list = [];
        for(Map i in data) {
          list.add(EstadoCita.fromJson(i));
        }
        _result.result = list;
        _result.code = ResponseResult.successCode;
      } else {
        _result.code = response.statusCode;
        _result.message = response.statusMessage;
      }
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> initAddEditPage() async{
    try {
      Response response = await _dio.get('Miscellaneous/GetDataToAddHome',
          options: buildCacheOptions(Duration(days: 7), forceRefresh: true));
      if(response.statusCode == 200) {
        dynamic data = response.data;
        List<TipoVivienda> listTipoViviendas = [];
        List<Pais> listPaises = [];
        for(Map i in data['tipoVivienda']) {
          listTipoViviendas.add(TipoVivienda.fromJson(i));
        }
        for(Map i in data['pais']) {
          listPaises.add(Pais.fromJson(i));
        }
        _result.result = {
          'tipoViviendas': listTipoViviendas,
          'paises': listPaises
        };
        _result.code = ResponseResult.successCode;
      } else {
        _result.code = response.statusCode;
        _result.message = response.statusMessage;
      }
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getDivision(int idPais) async{
    try {
      Response response = await _dio.get('Miscellaneous/GetDivisionByCountry',
          queryParameters: {'id': idPais},
          options: buildCacheOptions(Duration(days: 7), forceRefresh: true));
      if(response.statusCode == 200) {
        dynamic data = response.data;
        List<DivisionPais> list = [];
        for(Map i in data) {
          list.add(DivisionPais.fromJson(i));
        }
        _result.result = list;
        _result.code = ResponseResult.successCode;
      } else {
        _result.code = response.statusCode;
        _result.message = response.statusMessage;
      }
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getSubDivision(int idDivision) async{
    try {
      Response response = await _dio.get('Miscellaneous/GetSubDivisionByDivision',
          queryParameters: {'id': idDivision},
          options: buildCacheOptions(Duration(days: 7), forceRefresh: true));
      if(response.statusCode == 200) {
        dynamic data = response.data;
        List<SubdivisionPais> list = [];
        for(Map i in data) {
          list.add(SubdivisionPais.fromJson(i));
        }
        _result.result = list;
        _result.code = ResponseResult.successCode;
      } else {
        _result.code = response.statusCode;
        _result.message = response.statusMessage;
      }
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getCompletePlaceTree(int idSubdivision) async{
    try {
      Response response = await _dio.get('Miscellaneous/GetCompletePlaceTree',
          queryParameters: {'id': idSubdivision},
          options: buildCacheOptions(Duration(days: 7), forceRefresh: true)
      );
      if(response.statusCode == 200) {
        dynamic data = response.data;
        List<DivisionPais> listDivision = [];
        List<SubdivisionPais> listSubdivsion = [];
        List<Pais> listPaises = [];
        for(Map i in data['paises']) {
          listPaises.add(Pais.fromJson(i));
          if(!GetUtils.isNull(i['divisionPais'])) {
            for(Map j in i['divisionPais']) {
              listDivision.add(DivisionPais.fromJson(j));
              if(!GetUtils.isNull(j['subdivisionPais'])) {
                for(Map k in j['subdivisionPais']) {
                  listSubdivsion.add(SubdivisionPais.fromJson(k));
                }
              }
            }
          }
        }
        _result.result = {
          'paises': listPaises,
          'division': listDivision,
          'subdivision': listSubdivsion,
          'idDivision': data['idDivision'],
          'idPais': data['idPais']
        };
        _result.code = ResponseResult.successCode;
      } else {
        _result.code = response.statusCode;
        _result.message = response.statusMessage;
      }
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }
}