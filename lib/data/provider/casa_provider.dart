import 'package:dio/dio.dart';
import 'package:rentario_app/data/provider/adapter.dart';
import 'package:rentario_app/models/filter.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/casa.dart';
import 'dart:io';
import 'dart:convert' as convert;

class CasaProvider {
  Dio _dio = Adapter.getAdapter();
  ResponseResult _result = ResponseResult();

  Future<ResponseResult> saveHome(Casa casa, String token) async{
    try {
      dynamic data = casa.toJsonAdd();
      await _dio.post('ViviendasParaAlquilars',
        data: data,
        options: Options(
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json'
          }
        )
      );
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> saveChangesHome(Casa casa, String token) async{
    try {
      dynamic data = casa.toJsonEdit();
      await _dio.put('ViviendasParaAlquilars/${casa.id}',
          data: data,
          options: Options(
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          )
      );
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getHousesByPage(int idUsuario, int size, int page, String token, Filter filter) async{
    try {
      Response response;
      if(filter == null) {
        response = await _dio.get('ViviendasParaAlquilarAux/GetHouseByPage',
            options: Options(
                headers: {
                  HttpHeaders.authorizationHeader: 'Bearer $token',
                  HttpHeaders.contentTypeHeader: 'application/json'
                }
            ),
            queryParameters: {
              'page': page,
              'size': size,
              'idUsuario': idUsuario
            }
        );
      }
      else {
        String _filterString = convert.jsonEncode(filter.toJson());
        response = await _dio.get('ViviendasParaAlquilarAux/GetHouseByPageFiltered',
          options: Options(
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ),
          queryParameters: {
            'page': page,
            'size': size,
            'idUsuario': idUsuario,
            'filter': _filterString
          },
        );
      }
      _result.code = ResponseResult.successCode;
      dynamic data = response.data;
      List<Casa> list = [];
      for(Map i in data) {
        list.add(Casa.fromCustomJson(i));
      }
      _result.result = list;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getHousesByUser(int size, int page, int userId, String token) async{
    try {
      Response response = await _dio.get('ViviendasParaAlquilarAux/GetHouseByUser',
          options: Options(
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ),
          queryParameters: {'page': page, 'size': size, 'idUsuario': userId}
      );
      _result.code = ResponseResult.successCode;
      dynamic data = response.data;
      List<Casa> list = [];
      for(Map i in data) {
        list.add(Casa.fromCustomJson(i));
      }
      _result.result = list;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getHouse(int id, String token) async{
    try {
      Response response = await _dio.get('ViviendasParaAlquilars/$id',
          options: Options(
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ),
      );
      _result.code = ResponseResult.successCode;
      dynamic data = response.data;
      Casa casa = Casa.fromJson(data);
      _result.result = casa;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }
}