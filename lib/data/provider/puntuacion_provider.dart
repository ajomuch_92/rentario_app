import 'package:dio/dio.dart';
import 'package:rentario_app/data/provider/adapter.dart';
import 'package:rentario_app/models/puntuacion.dart';
import 'package:rentario_app/models/response_result.dart';
import 'dart:io';

class PuntuacionProvider {
  Dio _dio = Adapter.getAdapter();
  ResponseResult _result = ResponseResult();

  Future<ResponseResult> savePuntuacion(Puntuacion puntuacion, String token) async{
    try{
      await _dio.post('PuntuacionPorViviendas',
          data: puntuacion.toJson(),
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ));
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getPuntuaciones(int idVivienda, String token) async{
    try{
      Response response = await _dio.get('PuntuacionPorViviendas',
          queryParameters: {
            'idVivienda': idVivienda
          },
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ));
      List<Puntuacion> list = [];
      if(response.statusCode == 200) {
        dynamic data = response.data;
        for(Map i in data) {
          list.add(Puntuacion.fromJson(i));
        }
      }
      _result.code = ResponseResult.successCode;
      _result.result = list;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }
}