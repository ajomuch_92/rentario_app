import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:rentario_app/controllers/global_controller.dart';

class Adapter {
  static GlobalController _globalController = _getController();

  static Dio getAdapter() {
    bool isDev;
    if(_globalController != null)
      isDev = _globalController.dev;
    else
      isDev = false;
    Dio _dio = Dio();
    _dio.options.baseUrl = isDev? 'http://192.168.0.30/rentario_api/api/': 'http://3.139.76.71/rentario_api/api/';
    return _dio;
  }

  static GlobalController _getController() {
    try {
      return Get.find();
    }catch(error){
      return null;
    }
  }
}