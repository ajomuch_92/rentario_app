import 'package:dio/dio.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/usuario.dart';
import 'dart:io';
import 'adapter.dart';

class LoginProvider {
  Dio _dio = Adapter.getAdapter();
  ResponseResult _result = ResponseResult();

  Future<ResponseResult> createUser(Usuario usuario) async{
    try{
      Response response = await _dio.post('Usuarios', data: usuario.toJsonRegister(), options: Options(responseType: ResponseType.json));
      if(response.statusCode == 200) {
        _result.code = ResponseResult.successCode;
      } else {
        _result.code = response.statusCode;
        _result.message = response.statusMessage;
      }
    }on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    }catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> login(Usuario usuario) async{
    try{
      Response response = await _dio.post('Login', data: usuario.toJsonLogin(), options: Options(responseType: ResponseType.json));
      if(response.statusCode == 200) {
        _result.result = Usuario.fromJson(response.data);
        _result.code = ResponseResult.successCode;
      } else {
        _result.code = response.statusCode;
        _result.message = response.statusMessage;
      }
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getCode(String email) async{
    try{
      Response response = await _dio.get('Login',
        queryParameters: {'email': email},
        options: Options(responseType: ResponseType.json)
      );
      dynamic data = response.data;
      _result.result = {
        'code': data['code'],
        'jwt': data['jwt']
      };
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> resetPassword(Usuario usuario, String token) async{
    try{
      await _dio.post('Login/ResetPassword',
          data: usuario.toJsonRecovery(),
          options: Options(
            responseType: ResponseType.json,
            headers: {
              HttpHeaders.authorizationHeader: 'Bearer $token',
              HttpHeaders.contentTypeHeader: 'application/json'
            }
          )
      );
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }
}