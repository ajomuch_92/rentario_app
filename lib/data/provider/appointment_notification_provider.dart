import 'package:dio/dio.dart';
import 'dart:io';
import 'adapter.dart';
import 'package:rentario_app/data/provider/adapter.dart';
import 'package:rentario_app/models/cita.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/utils/constants_appointment.dart';
import 'package:intl/intl.dart';

class AppointmentNotificationProvider {
  Dio _dio = Adapter.getAdapter();
  ResponseResult _result = ResponseResult();

  Future<ResponseResult> saveAppointment(Cita cita, String token) async{
    try{
      cita.estado = send;
      await _dio.post('Citas',
          data: cita.toJson(),
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          )
      );
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getAppointments(int idUsuario, int page, int size, String token) async{
    try{
      Response response = await _dio.get('Citas/GetCitasByUser',
          queryParameters: {
            'idUsuario': idUsuario,
            'page': page,
            'size': size
          },
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          )
      );
      dynamic data = response.data;
      List<Cita> list = [];
      for(Map i in data) {
        list.add(Cita.fromJson(i));
      }
      _result.result = list;
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> getAppointmentsByClient(int idClient, int page, int size, String token) async{
    try{
      Response response = await _dio.get('Citas/GetCitasByClient',
          queryParameters: {
            'idClient': idClient,
            'page': page,
            'size': size
          },
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          )
      );
      dynamic data = response.data;
      List<Cita> list = [];
      for(Map i in data) {
        list.add(Cita.fromJson(i));
      }
      _result.result = list;
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }

  Future<ResponseResult> updateAppointment(Cita cita, String token) async{
    try{
      List<Map<String, dynamic>> data = [
        {
          'op': 'replace',
          'path': '/Estado',
          'value': cita.estado
        },
        {
          'op': 'replace',
          'path': '/Arrendatario',
          'value': cita.arrendatario
        }
      ];
      if(cita.fechaCita != null) {
        data.add(
          {
            'op': 'replace',
            'path': '/FechaCita',
            'value': DateFormat('yyyy-MM-ddTHH:mm:ss').format(cita.fechaCita)
          }
        );
      }
      await _dio.patch('Citas/${cita.id}',
          data: data,
          options: Options(
              responseType: ResponseType.json,
              headers: {
                HttpHeaders.authorizationHeader: 'Bearer $token',
                HttpHeaders.contentTypeHeader: 'application/json'
              }
          ));
      _result.code = ResponseResult.successCode;
    } on DioError catch(error) {
      _result.code = error.response.statusCode;
      _result.message = error.message;
    } catch(error) {
      _result.code = ResponseResult.errorCode;
      _result.message = error.toString();
    }
    return _result;
  }
}