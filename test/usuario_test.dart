import 'package:flutter_test/flutter_test.dart';
import 'package:rentario_app/models/usuario.dart';


void main() {
  test('Test for usuario class', ()  {
    Usuario usuario = Usuario(correo: 'test@mail.com', contrasenia: '12345678');
    dynamic json1 = usuario.toJsonLogin();
    expect(json1['correo'].toString().length, usuario.correo.length);
    dynamic json2 = usuario.toJson();
    expect(json2['contrasenia'], null);
  });
}