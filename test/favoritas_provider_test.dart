import 'package:flutter_test/flutter_test.dart';
import 'package:rentario_app/data/provider/favoritas_provider.dart';
import 'package:rentario_app/models/response_result.dart';

void main() {
  test('Test for favoritos provider', () async {
    final FavoritasProvider favoritasProvider = FavoritasProvider();
    ResponseResult responseResult = await favoritasProvider.getMyFavoritas(18, 1, 5, '');
    expect(responseResult.code, 401);
    responseResult = await favoritasProvider.addFavoritas(18, 1, '');
    expect(responseResult.code, 401);
  });
}