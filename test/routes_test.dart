import 'package:flutter_test/flutter_test.dart';
import 'package:rentario_app/controllers/recovery_controller.dart';
import 'package:rentario_app/views/recovery.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:get_test/get_test.dart';

void main() {
  getTest(
    "Test for routes",
    widgetTest: (tester) async {
      expect('/', Get.currentRoute);

      Get.to(Recovery());
      expect('/Recovery', Get.currentRoute);

      Get.back();

      expect('/', Get.currentRoute);
    },
  );
}