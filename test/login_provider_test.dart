import 'package:flutter_test/flutter_test.dart';
import 'package:rentario_app/data/provider/login_provider.dart';
import 'package:rentario_app/models/response_result.dart';
import 'package:rentario_app/models/usuario.dart';


void main() {
  test('Test for login provider', () async {
    final LoginProvider loginProvider = LoginProvider();
    Usuario usuario = Usuario(correo: 'test@mail.com', contrasenia: '12345678');
    ResponseResult responseResult = await loginProvider.login(usuario);
    expect(responseResult.code, 404);
    usuario = Usuario();
    responseResult = await loginProvider.createUser(usuario);
    expect(responseResult.code, 2);
  });
}